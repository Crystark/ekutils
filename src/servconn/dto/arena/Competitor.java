// 
// EkUtils 
// 

package servconn.dto.arena;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.google.gson.annotations.Expose;

public class Competitor
{
    @Expose
    private String Uid;
    @Expose
    private String NickName;
    @Expose
    private String Level;
    @Expose
    private String Win;
    @Expose
    private String Lose;
    @Expose
    private String Avatar;
    @Expose
    private String Sex;
    @Expose
    private Integer Rank;
    
    public String getUid() {
        return this.Uid;
    }
    
    public void setUid(final String Uid) {
        this.Uid = Uid;
    }
    
    public String getNickName() {
        return this.NickName;
    }
    
    public void setNickName(final String NickName) {
        this.NickName = NickName;
    }
    
    public String getLevel() {
        return this.Level;
    }
    
    public void setLevel(final String Level) {
        this.Level = Level;
    }
    
    public String getWin() {
        return this.Win;
    }
    
    public void setWin(final String Win) {
        this.Win = Win;
    }
    
    public String getLose() {
        return this.Lose;
    }
    
    public void setLose(final String Lose) {
        this.Lose = Lose;
    }
    
    public String getAvatar() {
        return this.Avatar;
    }
    
    public void setAvatar(final String Avatar) {
        this.Avatar = Avatar;
    }
    
    public String getSex() {
        return this.Sex;
    }
    
    public void setSex(final String Sex) {
        this.Sex = Sex;
    }
    
    public Integer getRank() {
        return this.Rank;
    }
    
    public void setRank(final Integer Rank) {
        this.Rank = Rank;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
