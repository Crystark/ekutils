// 
// EkUtils 
// 

package servconn.dto.login;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServerLoginUserInfo
{
    @SerializedName("access_token")
    @Expose
    private String accessToken;
    @Expose
    private String uin;
    @Expose
    private String nick;
    @Expose
    private Integer MUid;
    @Expose
    private Integer time;
    @Expose
    private String sign;
    @Expose
    private String ppsign;
    
    public String getAccessToken() {
        return this.accessToken;
    }
    
    public void setAccessToken(final String accessToken) {
        this.accessToken = accessToken;
    }
    
    public String getUin() {
        return this.uin;
    }
    
    public void setUin(final String uin) {
        this.uin = uin;
    }
    
    public String getNick() {
        return this.nick;
    }
    
    public void setNick(final String nick) {
        this.nick = nick;
    }
    
    public Integer getMUid() {
        return this.MUid;
    }
    
    public void setMUid(final Integer MUid) {
        this.MUid = MUid;
    }
    
    public Integer getTime() {
        return this.time;
    }
    
    public void setTime(final Integer time) {
        this.time = time;
    }
    
    public String getSign() {
        return this.sign;
    }
    
    public void setSign(final String sign) {
        this.sign = sign;
    }
    
    public String getPpsign() {
        return this.ppsign;
    }
    
    public void setPpsign(final String ppsign) {
        this.ppsign = ppsign;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
