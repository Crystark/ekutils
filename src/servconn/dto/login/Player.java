// 
// EkUtils 
// 

package servconn.dto.login;

import java.util.UUID;

public class Player
{
    private String uin;
    private String deviceToken;
    private String deviceId;
    private Integer authType;
    private Integer authSerial;
    
    public String getUin() {
        return this.uin;
    }
    
    public String getDeviceId() {
        return this.deviceId;
    }
    
    public void setDeviceId(final String deviceId) {
        this.deviceId = deviceId;
    }
    
    public void setUin(final String uin) {
        this.uin = uin;
    }
    
    public String getDeviceToken() {
        return this.deviceToken;
    }
    
    public void setDeviceToken(final String deviceToken) {
        this.deviceToken = deviceToken;
    }
    
    public Player() {
        if (this.deviceId == null) {
            this.deviceId = UUID.randomUUID().toString();
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("Player [uin=");
        builder.append(this.uin);
        builder.append(", deviceToken=");
        builder.append(this.deviceToken);
        builder.append(", deviceId=");
        builder.append(this.deviceId);
        builder.append(", authType=");
        builder.append(this.authType);
        builder.append(", authSerial=");
        builder.append(this.authSerial);
        builder.append("]");
        return builder.toString();
    }
    
    public Integer getAuthType() {
        return this.authType;
    }
    
    public void setAuthType(final Integer authType) {
        this.authType = authType;
    }
    
    public Integer getAuthSerial() {
        return this.authSerial;
    }
    
    public void setAuthSerial(final Integer authSerial) {
        this.authSerial = authSerial;
    }
}
