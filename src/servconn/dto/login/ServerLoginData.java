// 
// EkUtils 
// 

package servconn.dto.login;

import org.apache.commons.lang3.builder.ToStringBuilder;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServerLoginData
{
    @SerializedName("new")
    @Expose
    private Boolean _new;
    @Expose
    private ServerLoginGameServer current;
    @Expose
    private List<ServerLoginList> list;
    @Expose
    private ServerLoginUserInfo uinfo;
    
    public ServerLoginData() {
        this.list = new ArrayList<ServerLoginList>();
    }
    
    public Boolean getNew() {
        return this._new;
    }
    
    public void setNew(final Boolean _new) {
        this._new = _new;
    }
    
    public ServerLoginGameServer getCurrent() {
        return this.current;
    }
    
    public void setCurrent(final ServerLoginGameServer current) {
        this.current = current;
    }
    
    public List<ServerLoginList> getList() {
        return this.list;
    }
    
    public void setList(final List<ServerLoginList> list) {
        this.list = list;
    }
    
    public ServerLoginUserInfo getUinfo() {
        return this.uinfo;
    }
    
    public void setUinfo(final ServerLoginUserInfo uinfo) {
        this.uinfo = uinfo;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
