// 
// EkUtils 
// 

package servconn.dto.login;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.google.gson.annotations.Expose;

public class ServerLoginDto
{
    @Expose
    private Integer status;
    @Expose
    private ServerLoginData data;
    
    public Integer getStatus() {
        return this.status;
    }
    
    public void setStatus(final Integer status) {
        this.status = status;
    }
    
    public ServerLoginData getData() {
        return this.data;
    }
    
    public void setData(final ServerLoginData data) {
        this.data = data;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
