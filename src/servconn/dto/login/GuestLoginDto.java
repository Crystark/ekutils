// 
// EkUtils 
// 

package servconn.dto.login;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.google.gson.annotations.Expose;

public class GuestLoginDto
{
    @Expose
    private Boolean result;
    @Expose
    private String loginstatus;
    @Expose
    private String redirect;
    @Expose
    private String msg;
    
    public String getMsg() {
        return this.msg;
    }
    
    public void setMsg(final String msg) {
        this.msg = msg;
    }
    
    public String getRedirect() {
        return this.redirect;
    }
    
    public void setRedirect(final String redirect) {
        this.redirect = redirect;
    }
    
    public Boolean getResult() {
        return this.result;
    }
    
    public void setResult(final Boolean result) {
        this.result = result;
    }
    
    public String getLoginstatus() {
        return this.loginstatus;
    }
    
    public void setLoginstatus(final String loginstatus) {
        this.loginstatus = loginstatus;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
