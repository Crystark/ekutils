// 
// EkUtils 
// 

package servconn.dto.login;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServerLoginGameServer
{
    @SerializedName("GS_ID")
    @Expose
    private Integer GSID;
    @SerializedName("GS_STATUS")
    @Expose
    private Integer GSSTATUS;
    @SerializedName("GS_CHAT_IP")
    @Expose
    private String GSCHATIP;
    @SerializedName("GS_CHAT_PORT")
    @Expose
    private Integer GSCHATPORT;
    @SerializedName("GS_NAME")
    @Expose
    private String GSNAME;
    @SerializedName("GS_IP")
    @Expose
    private String GSIP;
    
    public Integer getGSID() {
        return this.GSID;
    }
    
    public void setGSID(final Integer GSID) {
        this.GSID = GSID;
    }
    
    public Integer getGSSTATUS() {
        return this.GSSTATUS;
    }
    
    public void setGSSTATUS(final Integer GSSTATUS) {
        this.GSSTATUS = GSSTATUS;
    }
    
    public String getGSCHATIP() {
        return this.GSCHATIP;
    }
    
    public void setGSCHATIP(final String GSCHATIP) {
        this.GSCHATIP = GSCHATIP;
    }
    
    public Integer getGSCHATPORT() {
        return this.GSCHATPORT;
    }
    
    public void setGSCHATPORT(final Integer GSCHATPORT) {
        this.GSCHATPORT = GSCHATPORT;
    }
    
    public String getGSNAME() {
        return this.GSNAME;
    }
    
    public void setGSNAME(final String GSNAME) {
        this.GSNAME = GSNAME;
    }
    
    public String getGSIP() {
        return this.GSIP;
    }
    
    public void setGSIP(final String GSIP) {
        this.GSIP = GSIP;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
