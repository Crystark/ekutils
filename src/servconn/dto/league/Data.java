// 
// EkUtils 
// 

package servconn.dto.league;

import org.apache.commons.lang3.builder.ToStringBuilder;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;

public class Data
{
    @Expose
    private LeagueNow LeagueNow;
    private transient LeagueNext LeagueNext;
    private transient List<Object> BetLogs;
    private transient List<Object> BetHistory;
    private transient LeagueRoundEndTime LeagueRoundEndTime;
    
    public Data() {
        this.BetLogs = new ArrayList<Object>();
        this.BetHistory = new ArrayList<Object>();
    }
    
    public LeagueNow getLeagueNow() {
        return this.LeagueNow;
    }
    
    public void setLeagueNow(final LeagueNow LeagueNow) {
        this.LeagueNow = LeagueNow;
    }
    
    public LeagueNext getLeagueNext() {
        return this.LeagueNext;
    }
    
    public void setLeagueNext(final LeagueNext LeagueNext) {
        this.LeagueNext = LeagueNext;
    }
    
    public List<Object> getBetLogs() {
        return this.BetLogs;
    }
    
    public void setBetLogs(final List<Object> BetLogs) {
        this.BetLogs = BetLogs;
    }
    
    public List<Object> getBetHistory() {
        return this.BetHistory;
    }
    
    public void setBetHistory(final List<Object> BetHistory) {
        this.BetHistory = BetHistory;
    }
    
    public LeagueRoundEndTime getLeagueRoundEndTime() {
        return this.LeagueRoundEndTime;
    }
    
    public void setLeagueRoundEndTime(final LeagueRoundEndTime LeagueRoundEndTime) {
        this.LeagueRoundEndTime = LeagueRoundEndTime;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
