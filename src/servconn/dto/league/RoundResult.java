// 
// EkUtils 
// 

package servconn.dto.league;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.google.gson.annotations.Expose;

public class RoundResult
{
    @Expose
    private String Uid;
    @Expose
    private String Opponent;
    @Expose
    private String BattleId;
    @Expose
    private BattleInfo BattleInfo;
    @Expose
    private Double BetOdds;
    @Expose
    private String BetTotal;
    
    public String getUid() {
        return this.Uid;
    }
    
    public void setUid(final String Uid) {
        this.Uid = Uid;
    }
    
    public String getOpponent() {
        return this.Opponent;
    }
    
    public void setOpponent(final String Opponent) {
        this.Opponent = Opponent;
    }
    
    public String getBattleId() {
        return this.BattleId;
    }
    
    public void setBattleId(final String BattleId) {
        this.BattleId = BattleId;
    }
    
    public BattleInfo getBattleInfo() {
        return this.BattleInfo;
    }
    
    public void setBattleInfo(final BattleInfo BattleInfo) {
        this.BattleInfo = BattleInfo;
    }
    
    public Double getBetOdds() {
        return this.BetOdds;
    }
    
    public void setBetOdds(final Double BetOdds) {
        this.BetOdds = BetOdds;
    }
    
    public String getBetTotal() {
        return this.BetTotal;
    }
    
    public void setBetTotal(final String BetTotal) {
        this.BetTotal = BetTotal;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
