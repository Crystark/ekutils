// 
// EkUtils 
// 

package servconn.dto.league;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.google.gson.annotations.Expose;

public class Rune
{
    @Expose
    private String UserRuneId;
    @Expose
    private String RuneId;
    @Expose
    private String Level;
    
    public String getUserRuneId() {
        return this.UserRuneId;
    }
    
    public void setUserRuneId(final String UserRuneId) {
        this.UserRuneId = UserRuneId;
    }
    
    public String getRuneId() {
        return this.RuneId;
    }
    
    public void setRuneId(final String RuneId) {
        this.RuneId = RuneId;
    }
    
    public String getLevel() {
        return this.Level;
    }
    
    public void setLevel(final String Level) {
        this.Level = Level;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
