// 
// EkUtils 
// 

package servconn.dto.league;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.google.gson.annotations.Expose;

public class Card
{
    @Expose
    private String UserCardId;
    @Expose
    private String CardId;
    @Expose
    private String Level;
    @Expose
    private String Evolution;
    @Expose
    private String SkillNew;
    @Expose
    private String WashTime;
    
    public String getUserCardId() {
        return this.UserCardId;
    }
    
    public void setUserCardId(final String UserCardId) {
        this.UserCardId = UserCardId;
    }
    
    public String getCardId() {
        return this.CardId;
    }
    
    public void setCardId(final String CardId) {
        this.CardId = CardId;
    }
    
    public String getLevel() {
        return this.Level;
    }
    
    public void setLevel(final String Level) {
        this.Level = Level;
    }
    
    public String getEvolution() {
        return this.Evolution;
    }
    
    public void setEvolution(final String Evolution) {
        this.Evolution = Evolution;
    }
    
    public String getSkillNew() {
        return this.SkillNew;
    }
    
    public void setSkillNew(final String SkillNew) {
        this.SkillNew = SkillNew;
    }
    
    public String getWashTime() {
        return this.WashTime;
    }
    
    public void setWashTime(final String WashTime) {
        this.WashTime = WashTime;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
