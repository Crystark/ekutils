// 
// EkUtils 
// 

package servconn.dto.league;

import org.apache.commons.lang3.builder.ToStringBuilder;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;

public class BattleInfo
{
    @Expose
    private User User;
    @Expose
    private List<Card> Cards;
    @Expose
    private List<Rune> Runes;
    
    public BattleInfo() {
        this.Cards = new ArrayList<Card>();
        this.Runes = new ArrayList<Rune>();
    }
    
    public User getUser() {
        return this.User;
    }
    
    public void setUser(final User User) {
        this.User = User;
    }
    
    public List<Card> getCards() {
        return this.Cards;
    }
    
    public void setCards(final List<Card> Cards) {
        this.Cards = Cards;
    }
    
    public List<Rune> getRunes() {
        return this.Runes;
    }
    
    public void setRunes(final List<Rune> Runes) {
        this.Runes = Runes;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
