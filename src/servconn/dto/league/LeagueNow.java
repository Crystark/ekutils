// 
// EkUtils 
// 

package servconn.dto.league;

import org.apache.commons.lang3.builder.ToStringBuilder;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;

public class LeagueNow
{
    @Expose
    private Integer LeagueId;
    private transient Integer ConditionId;
    @Expose
    private Integer RoundNow;
    @Expose
    private List<List<RoundResult>> RoundResult;
    @Expose
    private Integer Status;
    private transient Integer CreateTime;
    private transient Condition Condition;
    @Expose
    private Integer RemainTime;
    
    public LeagueNow() {
        this.RoundResult = new ArrayList<List<RoundResult>>();
    }
    
    public Integer getLeagueId() {
        return this.LeagueId;
    }
    
    public void setLeagueId(final Integer LeagueId) {
        this.LeagueId = LeagueId;
    }
    
    public Integer getConditionId() {
        return this.ConditionId;
    }
    
    public void setConditionId(final Integer ConditionId) {
        this.ConditionId = ConditionId;
    }
    
    public Integer getRoundNow() {
        return this.RoundNow;
    }
    
    public void setRoundNow(final Integer RoundNow) {
        this.RoundNow = RoundNow;
    }
    
    public List<List<RoundResult>> getRoundResult() {
        return this.RoundResult;
    }
    
    public void setRoundResult(final List<List<RoundResult>> RoundResult) {
        this.RoundResult = RoundResult;
    }
    
    public Integer getStatus() {
        return this.Status;
    }
    
    public void setStatus(final Integer Status) {
        this.Status = Status;
    }
    
    public Integer getCreateTime() {
        return this.CreateTime;
    }
    
    public void setCreateTime(final Integer CreateTime) {
        this.CreateTime = CreateTime;
    }
    
    public Condition getCondition() {
        return this.Condition;
    }
    
    public void setCondition(final Condition Condition) {
        this.Condition = Condition;
    }
    
    public Integer getRemainTime() {
        return this.RemainTime;
    }
    
    public void setRemainTime(final Integer RemainTime) {
        this.RemainTime = RemainTime;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
