// 
// EkUtils 
// 

package servconn.dto.friend;

import org.apache.commons.lang3.builder.ToStringBuilder;
import java.util.ArrayList;
import com.google.gson.annotations.Expose;
import java.util.List;

public class Data
{
    @Expose
    private List<Friend> Friends;
    
    public Data() {
        this.Friends = new ArrayList<Friend>();
    }
    
    public List<Friend> getFriends() {
        return this.Friends;
    }
    
    public void setFriends(final List<Friend> Friends) {
        this.Friends = Friends;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
