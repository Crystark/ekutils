// 
// EkUtils 
// 

package servconn.dto.friend;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.google.gson.annotations.Expose;

public class Friend
{
    @Expose
    private String Uid;
    @Expose
    private String NickName;
    @Expose
    private Integer Level;
    @Expose
    private Integer Avatar;
    @Expose
    private String Sex;
    @Expose
    private Integer Win;
    @Expose
    private Integer Lose;
    @Expose
    private Integer Rank;
    @Expose
    private String LastLogin;
    @Expose
    private String LegionName;
    @Expose
    private Integer Coins;
    @Expose
    private Integer FriendNum;
    @Expose
    private String FriendNumMax;
    @Expose
    private Integer FEnergySurplus;
    @Expose
    private Integer FEnergySend;
    
    public String getUid() {
        return this.Uid;
    }
    
    public void setUid(final String Uid) {
        this.Uid = Uid;
    }
    
    public String getNickName() {
        return this.NickName;
    }
    
    public void setNickName(final String NickName) {
        this.NickName = NickName;
    }
    
    public Integer getLevel() {
        return this.Level;
    }
    
    public void setLevel(final Integer Level) {
        this.Level = Level;
    }
    
    public Integer getAvatar() {
        return this.Avatar;
    }
    
    public void setAvatar(final Integer Avatar) {
        this.Avatar = Avatar;
    }
    
    public String getSex() {
        return this.Sex;
    }
    
    public void setSex(final String Sex) {
        this.Sex = Sex;
    }
    
    public Integer getWin() {
        return this.Win;
    }
    
    public void setWin(final Integer Win) {
        this.Win = Win;
    }
    
    public Integer getLose() {
        return this.Lose;
    }
    
    public void setLose(final Integer Lose) {
        this.Lose = Lose;
    }
    
    public Integer getRank() {
        return this.Rank;
    }
    
    public void setRank(final Integer Rank) {
        this.Rank = Rank;
    }
    
    public String getLastLogin() {
        return this.LastLogin;
    }
    
    public void setLastLogin(final String LastLogin) {
        this.LastLogin = LastLogin;
    }
    
    public String getLegionName() {
        return this.LegionName;
    }
    
    public void setLegionName(final String LegionName) {
        this.LegionName = LegionName;
    }
    
    public Integer getCoins() {
        return this.Coins;
    }
    
    public void setCoins(final Integer Coins) {
        this.Coins = Coins;
    }
    
    public Integer getFriendNum() {
        return this.FriendNum;
    }
    
    public void setFriendNum(final Integer FriendNum) {
        this.FriendNum = FriendNum;
    }
    
    public String getFriendNumMax() {
        return this.FriendNumMax;
    }
    
    public void setFriendNumMax(final String FriendNumMax) {
        this.FriendNumMax = FriendNumMax;
    }
    
    public Integer getFEnergySurplus() {
        return this.FEnergySurplus;
    }
    
    public void setFEnergySurplus(final Integer FEnergySurplus) {
        this.FEnergySurplus = FEnergySurplus;
    }
    
    public Integer getFEnergySend() {
        return this.FEnergySend;
    }
    
    public void setFEnergySend(final Integer FEnergySend) {
        this.FEnergySend = FEnergySend;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
