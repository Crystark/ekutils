// 
// EkUtils 
// 

package servconn.dto.skill;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.google.gson.annotations.Expose;

public class Skill
{
    @Expose
    private Integer SkillId;
    @Expose
    private String Name;
    @Expose
    private Integer Type;
    @Expose
    private Integer LanchType;
    @Expose
    private Integer LanchCondition;
    @Expose
    private Integer LanchConditionValue;
    @Expose
    private Integer AffectType;
    @Expose
    private String AffectValue;
    @Expose
    private String AffectValue2;
    @Expose
    private Integer SkillCategory;
    @Expose
    private String Desc;
    @Expose
    private Integer EvoRank;
    
    public Integer getSkillId() {
        return this.SkillId;
    }
    
    public void setSkillId(final Integer SkillId) {
        this.SkillId = SkillId;
    }
    
    public String getName() {
        return this.Name;
    }
    
    public void setName(final String Name) {
        this.Name = Name;
    }
    
    public Integer getType() {
        return this.Type;
    }
    
    public void setType(final Integer Type) {
        this.Type = Type;
    }
    
    public Integer getLanchType() {
        return this.LanchType;
    }
    
    public void setLanchType(final Integer LanchType) {
        this.LanchType = LanchType;
    }
    
    public Integer getLanchCondition() {
        return this.LanchCondition;
    }
    
    public void setLanchCondition(final Integer LanchCondition) {
        this.LanchCondition = LanchCondition;
    }
    
    public Integer getLanchConditionValue() {
        return this.LanchConditionValue;
    }
    
    public void setLanchConditionValue(final Integer LanchConditionValue) {
        this.LanchConditionValue = LanchConditionValue;
    }
    
    public Integer getAffectType() {
        return this.AffectType;
    }
    
    public void setAffectType(final Integer AffectType) {
        this.AffectType = AffectType;
    }
    
    public String getAffectValue() {
        return this.AffectValue;
    }
    
    public void setAffectValue(final String AffectValue) {
        this.AffectValue = AffectValue;
    }
    
    public String getAffectValue2() {
        return this.AffectValue2;
    }
    
    public void setAffectValue2(final String AffectValue2) {
        this.AffectValue2 = AffectValue2;
    }
    
    public Integer getSkillCategory() {
        return this.SkillCategory;
    }
    
    public void setSkillCategory(final Integer SkillCategory) {
        this.SkillCategory = SkillCategory;
    }
    
    public String getDesc() {
        return this.Desc;
    }
    
    public void setDesc(final String Desc) {
        this.Desc = Desc;
    }
    
    public Integer getEvoRank() {
        return this.EvoRank;
    }
    
    public void setEvoRank(final Integer EvoRank) {
        this.EvoRank = EvoRank;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
