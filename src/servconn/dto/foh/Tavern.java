// 
// EkUtils 
// 

package servconn.dto.foh;

import servconn.dto.common.Version;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Tavern
{
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private TavernData data;
    @SerializedName("version")
    @Expose
    private Version version;
    
    public Integer getStatus() {
        return this.status;
    }
    
    public void setStatus(final Integer status) {
        this.status = status;
    }
    
    public TavernData getData() {
        return this.data;
    }
    
    public void setData(final TavernData data) {
        this.data = data;
    }
    
    public Version getVersion() {
        return this.version;
    }
    
    public void setVersion(final Version version) {
        this.version = version;
    }
}
