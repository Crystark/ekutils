// 
// EkUtils 
// 

package servconn.dto.foh;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TavernAward
{
    @SerializedName("LeaguePoint")
    @Expose
    private Integer LeaguePoint;
    
    public Integer getLeaguePoint() {
        return this.LeaguePoint;
    }
    
    public void setLeaguePoint(final Integer LeaguePoint) {
        this.LeaguePoint = LeaguePoint;
    }
}
