// 
// EkUtils 
// 

package servconn.dto.ew;

import org.apache.commons.lang3.builder.ToStringBuilder;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EwBoss
{
    @SerializedName("totalHp")
    @Expose
    private Integer totalHp;
    @SerializedName("currHp")
    @Expose
    private Integer currHp;
    @SerializedName("Cards")
    @Expose
    private List<EwBossCard> Cards;
    
    public EwBoss() {
        this.Cards = new ArrayList<EwBossCard>();
    }
    
    public Integer getTotalHp() {
        return this.totalHp;
    }
    
    public void setTotalHp(final Integer totalHp) {
        this.totalHp = totalHp;
    }
    
    public Integer getCurrHp() {
        return this.currHp;
    }
    
    public void setCurrHp(final Integer currHp) {
        this.currHp = currHp;
    }
    
    public List<EwBossCard> getCards() {
        return this.Cards;
    }
    
    public void setCards(final List<EwBossCard> Cards) {
        this.Cards = Cards;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
