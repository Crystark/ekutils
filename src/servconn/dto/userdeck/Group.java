// 
// EkUtils 
// 

package servconn.dto.userdeck;

import org.apache.commons.lang3.builder.ToStringBuilder;
import java.util.ArrayList;
import servconn.dto.usercard.UserCard;
import java.util.List;
import com.google.gson.annotations.Expose;

public class Group
{
    @Expose
    private Integer Uid;
    @Expose
    private String UserCardIds;
    @Expose
    private String UserRuneIds;
    @Expose
    private Integer GroupId;
    @Expose
    private List<UserCard> UserCardInfo;
    @Expose
    private List<UserRuneInfo> UserRuneInfo;
    
    public Group() {
        this.UserCardInfo = new ArrayList<UserCard>();
        this.UserRuneInfo = new ArrayList<UserRuneInfo>();
    }
    
    public Integer getUid() {
        return this.Uid;
    }
    
    public void setUid(final Integer Uid) {
        this.Uid = Uid;
    }
    
    public String getUserCardIds() {
        return this.UserCardIds;
    }
    
    public void setUserCardIds(final String UserCardIds) {
        this.UserCardIds = UserCardIds;
    }
    
    public String getUserRuneIds() {
        return this.UserRuneIds;
    }
    
    public void setUserRuneIds(final String UserRuneIds) {
        this.UserRuneIds = UserRuneIds;
    }
    
    public Integer getGroupId() {
        return this.GroupId;
    }
    
    public void setGroupId(final Integer GroupId) {
        this.GroupId = GroupId;
    }
    
    public List<UserCard> getUserCardInfo() {
        return this.UserCardInfo;
    }
    
    public void setUserCardInfo(final List<UserCard> UserCardInfo) {
        this.UserCardInfo = UserCardInfo;
    }
    
    public List<UserRuneInfo> getUserRuneInfo() {
        return this.UserRuneInfo;
    }
    
    public void setUserRuneInfo(final List<UserRuneInfo> UserRuneInfo) {
        this.UserRuneInfo = UserRuneInfo;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
