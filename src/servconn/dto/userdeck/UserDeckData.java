// 
// EkUtils 
// 

package servconn.dto.userdeck;

import org.apache.commons.lang3.builder.ToStringBuilder;
import java.util.ArrayList;
import com.google.gson.annotations.Expose;
import java.util.List;

public class UserDeckData
{
    @Expose
    private List<Group> Groups;
    @Expose
    private Integer legionWarGroupId;
    @Expose
    private Integer defenceGroupid;
    @Expose
    private Integer RmbGroupNum;
    @Expose
    private Integer RmbGroupCash;
    
    public UserDeckData() {
        this.Groups = new ArrayList<Group>();
    }
    
    public List<Group> getGroups() {
        return this.Groups;
    }
    
    public void setGroups(final List<Group> Groups) {
        this.Groups = Groups;
    }
    
    public Integer getLegionWarGroupId() {
        return this.legionWarGroupId;
    }
    
    public void setLegionWarGroupId(final Integer legionWarGroupId) {
        this.legionWarGroupId = legionWarGroupId;
    }
    
    public Integer getDefenceGroupid() {
        return this.defenceGroupid;
    }
    
    public void setDefenceGroupid(final Integer defenceGroupid) {
        this.defenceGroupid = defenceGroupid;
    }
    
    public Integer getRmbGroupNum() {
        return this.RmbGroupNum;
    }
    
    public void setRmbGroupNum(final Integer RmbGroupNum) {
        this.RmbGroupNum = RmbGroupNum;
    }
    
    public Integer getRmbGroupCash() {
        return this.RmbGroupCash;
    }
    
    public void setRmbGroupCash(final Integer RmbGroupCash) {
        this.RmbGroupCash = RmbGroupCash;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
