// 
// EkUtils 
// 

package servconn.dto.userdeck;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.google.gson.annotations.Expose;

public class UserRuneInfo
{
    @Expose
    private String UserRuneId;
    @Expose
    private String Uid;
    @Expose
    private String RuneId;
    @Expose
    private String Level;
    @Expose
    private String Exp;
    @Expose
    private Object Valid;
    
    public String getUserRuneId() {
        return this.UserRuneId;
    }
    
    public void setUserRuneId(final String UserRuneId) {
        this.UserRuneId = UserRuneId;
    }
    
    public String getUid() {
        return this.Uid;
    }
    
    public void setUid(final String Uid) {
        this.Uid = Uid;
    }
    
    public String getRuneId() {
        return this.RuneId;
    }
    
    public void setRuneId(final String RuneId) {
        this.RuneId = RuneId;
    }
    
    public String getLevel() {
        return this.Level;
    }
    
    public void setLevel(final String Level) {
        this.Level = Level;
    }
    
    public String getExp() {
        return this.Exp;
    }
    
    public void setExp(final String Exp) {
        this.Exp = Exp;
    }
    
    public Object getValid() {
        return this.Valid;
    }
    
    public void setValid(final Object Valid) {
        this.Valid = Valid;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
