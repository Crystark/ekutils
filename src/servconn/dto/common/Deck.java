// 
// EkUtils 
// 

package servconn.dto.common;

import java.util.List;

public class Deck
{
    private String deckId;
    private String deckFileName;
    private String playerName;
    private String playerLevel;
    private List<String> cardList;
    private List<String> runeList;
    
    public String getDeckId() {
        return this.deckId;
    }
    
    public void setDeckId(final String deckId) {
        this.deckId = deckId;
    }
    
    public String getDeckFileName() {
        return this.deckFileName;
    }
    
    public void setDeckFileName(final String deckFileName) {
        this.deckFileName = deckFileName;
    }
    
    public String getPlayerName() {
        return this.playerName;
    }
    
    public void setPlayerName(final String playerName) {
        this.playerName = playerName;
    }
    
    public String getPlayerLevel() {
        return this.playerLevel;
    }
    
    public void setPlayerLevel(final String playerLevel) {
        this.playerLevel = playerLevel;
    }
    
    public List<String> getCardList() {
        return this.cardList;
    }
    
    public void setCardList(final List<String> cardList) {
        this.cardList = cardList;
    }
    
    public List<String> getRuneList() {
        return this.runeList;
    }
    
    public void setRuneList(final List<String> runeList) {
        this.runeList = runeList;
    }
}
