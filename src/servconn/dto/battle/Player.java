// 
// EkUtils 
// 

package servconn.dto.battle;

import org.apache.commons.lang3.builder.ToStringBuilder;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;

public class Player
{
    @Expose
    private Integer Uid;
    @Expose
    private String NickName;
    @Expose
    private String Avatar;
    @Expose
    private Integer Sex;
    @Expose
    private String Level;
    @Expose
    private String HP;
    @Expose
    private List<BattleCard> Cards;
    @Expose
    private List<BattleRune> Runes;
    @Expose
    private Integer RemainHP;
    
    public Player() {
        this.Cards = new ArrayList<BattleCard>();
        this.Runes = new ArrayList<BattleRune>();
    }
    
    public Integer getUid() {
        return this.Uid;
    }
    
    public void setUid(final Integer Uid) {
        this.Uid = Uid;
    }
    
    public String getNickName() {
        return this.NickName;
    }
    
    public void setNickName(final String NickName) {
        this.NickName = NickName;
    }
    
    public String getAvatar() {
        return this.Avatar;
    }
    
    public void setAvatar(final String Avatar) {
        this.Avatar = Avatar;
    }
    
    public Integer getSex() {
        return this.Sex;
    }
    
    public void setSex(final Integer Sex) {
        this.Sex = Sex;
    }
    
    public String getLevel() {
        return this.Level;
    }
    
    public void setLevel(final String Level) {
        this.Level = Level;
    }
    
    public String getHP() {
        return this.HP;
    }
    
    public void setHP(final String HP) {
        this.HP = HP;
    }
    
    public List<BattleCard> getCards() {
        return this.Cards;
    }
    
    public void setCards(final List<BattleCard> Cards) {
        this.Cards = Cards;
    }
    
    public List<BattleRune> getRunes() {
        return this.Runes;
    }
    
    public void setRunes(final List<BattleRune> Runes) {
        this.Runes = Runes;
    }
    
    public Integer getRemainHP() {
        return this.RemainHP;
    }
    
    public void setRemainHP(final Integer RemainHP) {
        this.RemainHP = RemainHP;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
