// 
// EkUtils 
// 

package servconn.dto.battle;

import org.apache.commons.lang3.builder.ToStringBuilder;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;

public class Battle
{
    @Expose
    private Integer Round;
    @Expose
    private Boolean isAttack;
    @Expose
    private List<Opp> Opps;
    
    public Battle() {
        this.Opps = new ArrayList<Opp>();
    }
    
    public Integer getRound() {
        return this.Round;
    }
    
    public void setRound(final Integer Round) {
        this.Round = Round;
    }
    
    public Boolean getIsAttack() {
        return this.isAttack;
    }
    
    public void setIsAttack(final Boolean isAttack) {
        this.isAttack = isAttack;
    }
    
    public List<Opp> getOpps() {
        return this.Opps;
    }
    
    public void setOpps(final List<Opp> Opps) {
        this.Opps = Opps;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
