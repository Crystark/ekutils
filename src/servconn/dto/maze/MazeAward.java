// 
// EkUtils 
// 

package servconn.dto.maze;

import org.apache.commons.lang3.builder.ToStringBuilder;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;

public class MazeAward
{
    @Expose
    private Integer Coins;
    @Expose
    private Integer Exp;
    @Expose
    private Integer CardId;
    @Expose
    private List<String> SecondDrop;
    @Expose
    private MazeAwardCardChip CardChip;
    
    public MazeAward() {
        this.SecondDrop = new ArrayList<String>();
    }
    
    public Integer getCoins() {
        return this.Coins;
    }
    
    public void setCoins(final Integer Coins) {
        this.Coins = Coins;
    }
    
    public Integer getExp() {
        return this.Exp;
    }
    
    public void setExp(final Integer Exp) {
        this.Exp = Exp;
    }
    
    public Integer getCardId() {
        return this.CardId;
    }
    
    public void setCardId(final Integer CardId) {
        this.CardId = CardId;
    }
    
    public List<String> getSecondDrop() {
        return this.SecondDrop;
    }
    
    public void setSecondDrop(final List<String> SecondDrop) {
        this.SecondDrop = SecondDrop;
    }
    
    public MazeAwardCardChip getCardChip() {
        return this.CardChip;
    }
    
    public void setCardChip(final MazeAwardCardChip cardChip) {
        this.CardChip = cardChip;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
