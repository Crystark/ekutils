// 
// EkUtils 
// 

package servconn.dto.maze;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.google.gson.annotations.Expose;

public class MazeInfoData
{
    @Expose
    private String Name;
    @Expose
    private Integer Layer;
    @Expose
    private Integer Clear;
    @Expose
    private Integer FreeReset;
    @Expose
    private Integer RmbReset;
    @Expose
    private Integer ResetCash;
    @Expose
    private Integer OneKeyCash;
    
    public String getName() {
        return this.Name;
    }
    
    public void setName(final String Name) {
        this.Name = Name;
    }
    
    public Integer getLayer() {
        return this.Layer;
    }
    
    public void setLayer(final Integer Layer) {
        this.Layer = Layer;
    }
    
    public Integer getClear() {
        return this.Clear;
    }
    
    public void setClear(final Integer Clear) {
        this.Clear = Clear;
    }
    
    public Integer getFreeReset() {
        return this.FreeReset;
    }
    
    public void setFreeReset(final Integer FreeReset) {
        this.FreeReset = FreeReset;
    }
    
    public Integer getRmbReset() {
        return this.RmbReset;
    }
    
    public void setRmbReset(final Integer RmbReset) {
        this.RmbReset = RmbReset;
    }
    
    public Integer getResetCash() {
        return this.ResetCash;
    }
    
    public void setResetCash(final Integer ResetCash) {
        this.ResetCash = ResetCash;
    }
    
    public Integer getOneKeyCash() {
        return this.OneKeyCash;
    }
    
    public void setOneKeyCash(final Integer OneKeyCash) {
        this.OneKeyCash = OneKeyCash;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
