// 
// EkUtils 
// 

package servconn.dto.maze;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.google.gson.annotations.Expose;

public class MazeClear
{
    @Expose
    private Integer IsClear;
    @Expose
    private Integer CardId;
    @Expose
    private Integer Coins;
    
    public Integer getIsClear() {
        return this.IsClear;
    }
    
    public void setIsClear(final Integer IsClear) {
        this.IsClear = IsClear;
    }
    
    public Integer getCardId() {
        return this.CardId;
    }
    
    public void setCardId(final Integer CardId) {
        this.CardId = CardId;
    }
    
    public Integer getCoins() {
        return this.Coins;
    }
    
    public void setCoins(final Integer Coins) {
        this.Coins = Coins;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
