// 
// EkUtils 
// 

package servconn.dto.maze;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.google.gson.annotations.Expose;

public class MazeExtData
{
    @Expose
    private MazeAward Award;
    @Expose
    private MazeClear Clear;
    private transient MazeUser User;
    
    public MazeAward getAward() {
        return this.Award;
    }
    
    public void setAward(final MazeAward Award) {
        this.Award = Award;
    }
    
    public MazeClear getClear() {
        return this.Clear;
    }
    
    public void setClear(final MazeClear Clear) {
        this.Clear = Clear;
    }
    
    public MazeUser getUser() {
        return this.User;
    }
    
    public void setUser(final MazeUser User) {
        this.User = User;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
