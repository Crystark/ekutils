// 
// EkUtils 
// 

package servconn.dto.maze;

import servconn.dto.common.Version;
import com.google.gson.annotations.Expose;

public class MazeFloorInfo
{
    @Expose
    private Integer status;
    @Expose
    private MazeFloorInfoData data;
    @Expose
    private Version version;
    
    public Integer getStatus() {
        return this.status;
    }
    
    public void setStatus(final Integer status) {
        this.status = status;
    }
    
    public MazeFloorInfoData getData() {
        return this.data;
    }
    
    public void setData(final MazeFloorInfoData data) {
        this.data = data;
    }
    
    public Version getVersion() {
        return this.version;
    }
    
    public void setVersion(final Version version) {
        this.version = version;
    }
}
