// 
// EkUtils 
// 

package servconn.dto.buy;

import org.apache.commons.lang3.builder.ToStringBuilder;
import servconn.dto.common.Version;
import com.google.gson.annotations.Expose;

public class BuyingData
{
    @Expose
    private Integer status;
    @Expose
    private BuyingResult data;
    @Expose
    private Version version;
    
    public Integer getStatus() {
        return this.status;
    }
    
    public void setStatus(final Integer status) {
        this.status = status;
    }
    
    public BuyingResult getData() {
        return this.data;
    }
    
    public void setData(final BuyingResult data) {
        this.data = data;
    }
    
    public Version getVersion() {
        return this.version;
    }
    
    public void setVersion(final Version version) {
        this.version = version;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
