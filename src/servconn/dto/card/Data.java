// 
// EkUtils 
// 

package servconn.dto.card;

import org.apache.commons.lang3.builder.ToStringBuilder;
import java.util.ArrayList;
import com.google.gson.annotations.Expose;
import java.util.List;

public class Data
{
    @Expose
    private List<Card> Cards;
    
    public Data() {
        this.Cards = new ArrayList<Card>();
    }
    
    public List<Card> getCards() {
        return this.Cards;
    }
    
    public void setCards(final List<Card> Cards) {
        this.Cards = Cards;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
