// 
// EkUtils 
// 

package servconn.dto.card;

import java.util.ArrayList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

public class CollectionData
{
    @SerializedName("cardCollect")
    @Expose
    private List<CardCollect> cardCollect;
    
    public CollectionData() {
        this.cardCollect = new ArrayList<CardCollect>();
    }
    
    public List<CardCollect> getCardCollect() {
        return this.cardCollect;
    }
    
    public void setCardCollect(final List<CardCollect> cardCollect) {
        this.cardCollect = cardCollect;
    }
}
