// 
// EkUtils 
// 

package servconn.dto.card;

import org.apache.commons.lang3.builder.ToStringBuilder;
import ekdao.util.CardFaction;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;

public class Card
{
    @Expose
    private String CardId;
    @Expose
    private String CardName;
    @Expose
    private String Cost;
    @Expose
    private String Color;
    @Expose
    private String Race;
    @Expose
    private String Attack;
    @Expose
    private String Wait;
    @Expose
    private String Skill;
    @Expose
    private String LockSkill1;
    @Expose
    private String LockSkill2;
    @Expose
    private String ImageId;
    @Expose
    private String FullImageId;
    @Expose
    private String Price;
    @Expose
    private String BaseExp;
    @Expose
    private String MaxInDeck;
    @Expose
    private String SeniorPacket;
    @Expose
    private String MasterPacket;
    @Expose
    private String Maze;
    @Expose
    private String Robber;
    @Expose
    private String MagicCard;
    @Expose
    private String Boss;
    @Expose
    private String BossCounter;
    @Expose
    private String FactionCounter;
    @Expose
    private String Glory;
    @Expose
    private String FightMPacket;
    @Expose
    private String BigYearPacket;
    @Expose
    private String BigYearPacketRoll;
    @Expose
    private String StandardPacket;
    @Expose
    private String StandardPacketRoll;
    @Expose
    private String GuruPacket;
    @Expose
    private String GuruPacketRoll;
    @Expose
    private String ActivityRobber;
    @Expose
    private String ForceFightAuction;
    @Expose
    private String ForceAuctionInitPrice;
    @Expose
    private String ForceFightExchange;
    @Expose
    private String ForceExchangeInitPrice;
    @Expose
    private String IcePacket;
    @Expose
    private String ForestPacket;
    @Expose
    private String SwampPacket;
    @Expose
    private String VolcanoPacket;
    @Expose
    private String IcePacketRoll;
    @Expose
    private String ForestPacketRoll;
    @Expose
    private String SwampPacketRoll;
    @Expose
    private String VolcanoPacketRoll;
    @Expose
    private String CanDecompose;
    @Expose
    private String Rank;
    @Expose
    private String Fragment;
    @Expose
    private String ComposePrice;
    @Expose
    private String DecomposeGet;
    @Expose
    private String ThieveChipWeight;
    @Expose
    private String MazeChipWeight;
    @Expose
    private String CanEvo;
    @Expose
    private String KeyCard;
    @Expose
    private String LeagueWeight;
    @Expose
    private String LeaguePoint;
    @Expose
    private List<Integer> HpArray;
    @Expose
    private List<Integer> AttackArray;
    @Expose
    private List<String> ExpArray;
    
    public Card() {
        this.HpArray = new ArrayList<Integer>();
        this.AttackArray = new ArrayList<Integer>();
        this.ExpArray = new ArrayList<String>();
    }
    
    public boolean isSpecial() {
        return "99".equals(this.Race);
    }
    
    public boolean isGold() {
        return "95".equals(this.Race);
    }
    
    public boolean isFeast() {
        return "96".equals(this.Race);
    }
    
    public boolean isDemon() {
        return "100".equals(this.Race);
    }
    
    public boolean isHydra() {
        return "97".equals(this.Race) && !this.CardName.contains("Legendary");
    }
    
    public boolean isLegendary() {
        return "97".equals(this.Race) && this.CardName.contains("Legendary");
    }
    
    public boolean isTundra() {
        return CardFaction.tundra.getValue().equals(this.Race);
    }
    
    public boolean isForest() {
        return CardFaction.forest.getValue().equals(this.Race);
    }
    
    public boolean isMountain() {
        return CardFaction.mtn.getValue().equals(this.Race);
    }
    
    public boolean isSwamp() {
        return CardFaction.swamp.getValue().equals(this.Race);
    }
    
    public String getCardId() {
        return this.CardId;
    }
    
    public void setCardId(final String CardId) {
        this.CardId = CardId;
    }
    
    public String getCardName() {
        return this.CardName;
    }
    
    public void setCardName(final String CardName) {
        this.CardName = CardName;
    }
    
    public String getCost() {
        return this.Cost;
    }
    
    public void setCost(final String Cost) {
        this.Cost = Cost;
    }
    
    public String getColor() {
        return this.Color;
    }
    
    public void setColor(final String Color) {
        this.Color = Color;
    }
    
    public String getRace() {
        return this.Race;
    }
    
    public void setRace(final String Race) {
        this.Race = Race;
    }
    
    public String getAttack() {
        return this.Attack;
    }
    
    public void setAttack(final String Attack) {
        this.Attack = Attack;
    }
    
    public String getWait() {
        return this.Wait;
    }
    
    public void setWait(final String Wait) {
        this.Wait = Wait;
    }
    
    public String getSkill() {
        return this.Skill;
    }
    
    public void setSkill(final String Skill) {
        this.Skill = Skill;
    }
    
    public String getLockSkill1() {
        return this.LockSkill1;
    }
    
    public void setLockSkill1(final String LockSkill1) {
        this.LockSkill1 = LockSkill1;
    }
    
    public String getLockSkill2() {
        return this.LockSkill2;
    }
    
    public void setLockSkill2(final String LockSkill2) {
        this.LockSkill2 = LockSkill2;
    }
    
    public String getImageId() {
        return this.ImageId;
    }
    
    public void setImageId(final String ImageId) {
        this.ImageId = ImageId;
    }
    
    public String getFullImageId() {
        return this.FullImageId;
    }
    
    public void setFullImageId(final String FullImageId) {
        this.FullImageId = FullImageId;
    }
    
    public String getPrice() {
        return this.Price;
    }
    
    public void setPrice(final String Price) {
        this.Price = Price;
    }
    
    public String getBaseExp() {
        return this.BaseExp;
    }
    
    public void setBaseExp(final String BaseExp) {
        this.BaseExp = BaseExp;
    }
    
    public String getMaxInDeck() {
        return this.MaxInDeck;
    }
    
    public void setMaxInDeck(final String MaxInDeck) {
        this.MaxInDeck = MaxInDeck;
    }
    
    public String getSeniorPacket() {
        return this.SeniorPacket;
    }
    
    public void setSeniorPacket(final String SeniorPacket) {
        this.SeniorPacket = SeniorPacket;
    }
    
    public String getMasterPacket() {
        return this.MasterPacket;
    }
    
    public void setMasterPacket(final String MasterPacket) {
        this.MasterPacket = MasterPacket;
    }
    
    public String getMaze() {
        return this.Maze;
    }
    
    public void setMaze(final String Maze) {
        this.Maze = Maze;
    }
    
    public String getRobber() {
        return this.Robber;
    }
    
    public void setRobber(final String Robber) {
        this.Robber = Robber;
    }
    
    public String getMagicCard() {
        return this.MagicCard;
    }
    
    public void setMagicCard(final String MagicCard) {
        this.MagicCard = MagicCard;
    }
    
    public String getBoss() {
        return this.Boss;
    }
    
    public void setBoss(final String Boss) {
        this.Boss = Boss;
    }
    
    public String getBossCounter() {
        return this.BossCounter;
    }
    
    public void setBossCounter(final String BossCounter) {
        this.BossCounter = BossCounter;
    }
    
    public String getFactionCounter() {
        return this.FactionCounter;
    }
    
    public void setFactionCounter(final String FactionCounter) {
        this.FactionCounter = FactionCounter;
    }
    
    public String getGlory() {
        return this.Glory;
    }
    
    public void setGlory(final String Glory) {
        this.Glory = Glory;
    }
    
    public String getFightMPacket() {
        return this.FightMPacket;
    }
    
    public void setFightMPacket(final String FightMPacket) {
        this.FightMPacket = FightMPacket;
    }
    
    public String getBigYearPacket() {
        return this.BigYearPacket;
    }
    
    public void setBigYearPacket(final String BigYearPacket) {
        this.BigYearPacket = BigYearPacket;
    }
    
    public String getBigYearPacketRoll() {
        return this.BigYearPacketRoll;
    }
    
    public void setBigYearPacketRoll(final String BigYearPacketRoll) {
        this.BigYearPacketRoll = BigYearPacketRoll;
    }
    
    public String getStandardPacket() {
        return this.StandardPacket;
    }
    
    public void setStandardPacket(final String StandardPacket) {
        this.StandardPacket = StandardPacket;
    }
    
    public String getStandardPacketRoll() {
        return this.StandardPacketRoll;
    }
    
    public void setStandardPacketRoll(final String StandardPacketRoll) {
        this.StandardPacketRoll = StandardPacketRoll;
    }
    
    public String getGuruPacket() {
        return this.GuruPacket;
    }
    
    public void setGuruPacket(final String GuruPacket) {
        this.GuruPacket = GuruPacket;
    }
    
    public String getGuruPacketRoll() {
        return this.GuruPacketRoll;
    }
    
    public void setGuruPacketRoll(final String GuruPacketRoll) {
        this.GuruPacketRoll = GuruPacketRoll;
    }
    
    public String getActivityRobber() {
        return this.ActivityRobber;
    }
    
    public void setActivityRobber(final String ActivityRobber) {
        this.ActivityRobber = ActivityRobber;
    }
    
    public String getForceFightAuction() {
        return this.ForceFightAuction;
    }
    
    public void setForceFightAuction(final String ForceFightAuction) {
        this.ForceFightAuction = ForceFightAuction;
    }
    
    public String getForceAuctionInitPrice() {
        return this.ForceAuctionInitPrice;
    }
    
    public void setForceAuctionInitPrice(final String ForceAuctionInitPrice) {
        this.ForceAuctionInitPrice = ForceAuctionInitPrice;
    }
    
    public String getForceFightExchange() {
        return this.ForceFightExchange;
    }
    
    public void setForceFightExchange(final String ForceFightExchange) {
        this.ForceFightExchange = ForceFightExchange;
    }
    
    public String getForceExchangeInitPrice() {
        return this.ForceExchangeInitPrice;
    }
    
    public void setForceExchangeInitPrice(final String ForceExchangeInitPrice) {
        this.ForceExchangeInitPrice = ForceExchangeInitPrice;
    }
    
    public String getIcePacket() {
        return this.IcePacket;
    }
    
    public void setIcePacket(final String IcePacket) {
        this.IcePacket = IcePacket;
    }
    
    public String getForestPacket() {
        return this.ForestPacket;
    }
    
    public void setForestPacket(final String ForestPacket) {
        this.ForestPacket = ForestPacket;
    }
    
    public String getSwampPacket() {
        return this.SwampPacket;
    }
    
    public void setSwampPacket(final String SwampPacket) {
        this.SwampPacket = SwampPacket;
    }
    
    public String getVolcanoPacket() {
        return this.VolcanoPacket;
    }
    
    public void setVolcanoPacket(final String VolcanoPacket) {
        this.VolcanoPacket = VolcanoPacket;
    }
    
    public String getIcePacketRoll() {
        return this.IcePacketRoll;
    }
    
    public void setIcePacketRoll(final String IcePacketRoll) {
        this.IcePacketRoll = IcePacketRoll;
    }
    
    public String getForestPacketRoll() {
        return this.ForestPacketRoll;
    }
    
    public void setForestPacketRoll(final String ForestPacketRoll) {
        this.ForestPacketRoll = ForestPacketRoll;
    }
    
    public String getSwampPacketRoll() {
        return this.SwampPacketRoll;
    }
    
    public void setSwampPacketRoll(final String SwampPacketRoll) {
        this.SwampPacketRoll = SwampPacketRoll;
    }
    
    public String getVolcanoPacketRoll() {
        return this.VolcanoPacketRoll;
    }
    
    public void setVolcanoPacketRoll(final String VolcanoPacketRoll) {
        this.VolcanoPacketRoll = VolcanoPacketRoll;
    }
    
    public String getCanDecompose() {
        return this.CanDecompose;
    }
    
    public void setCanDecompose(final String CanDecompose) {
        this.CanDecompose = CanDecompose;
    }
    
    public String getRank() {
        return this.Rank;
    }
    
    public void setRank(final String Rank) {
        this.Rank = Rank;
    }
    
    public String getFragment() {
        return this.Fragment;
    }
    
    public void setFragment(final String Fragment) {
        this.Fragment = Fragment;
    }
    
    public String getComposePrice() {
        return this.ComposePrice;
    }
    
    public void setComposePrice(final String ComposePrice) {
        this.ComposePrice = ComposePrice;
    }
    
    public String getDecomposeGet() {
        return this.DecomposeGet;
    }
    
    public void setDecomposeGet(final String DecomposeGet) {
        this.DecomposeGet = DecomposeGet;
    }
    
    public String getThieveChipWeight() {
        return this.ThieveChipWeight;
    }
    
    public void setThieveChipWeight(final String ThieveChipWeight) {
        this.ThieveChipWeight = ThieveChipWeight;
    }
    
    public String getMazeChipWeight() {
        return this.MazeChipWeight;
    }
    
    public void setMazeChipWeight(final String MazeChipWeight) {
        this.MazeChipWeight = MazeChipWeight;
    }
    
    public String getCanEvo() {
        return this.CanEvo;
    }
    
    public void setCanEvo(final String CanEvo) {
        this.CanEvo = CanEvo;
    }
    
    public String getKeyCard() {
        return this.KeyCard;
    }
    
    public void setKeyCard(final String KeyCard) {
        this.KeyCard = KeyCard;
    }
    
    public String getLeagueWeight() {
        return this.LeagueWeight;
    }
    
    public void setLeagueWeight(final String LeagueWeight) {
        this.LeagueWeight = LeagueWeight;
    }
    
    public String getLeaguePoint() {
        return this.LeaguePoint;
    }
    
    public void setLeaguePoint(final String LeaguePoint) {
        this.LeaguePoint = LeaguePoint;
    }
    
    public List<Integer> getHpArray() {
        return this.HpArray;
    }
    
    public void setHpArray(final List<Integer> HpArray) {
        this.HpArray = HpArray;
    }
    
    public List<Integer> getAttackArray() {
        return this.AttackArray;
    }
    
    public void setAttackArray(final List<Integer> AttackArray) {
        this.AttackArray = AttackArray;
    }
    
    public List<String> getExpArray() {
        return this.ExpArray;
    }
    
    public void setExpArray(final List<String> ExpArray) {
        this.ExpArray = ExpArray;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
