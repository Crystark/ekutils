// 
// EkUtils 
// 

package servconn.dto.card;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.google.gson.annotations.Expose;

public class CardFragment
{
    @Expose
    private Integer CardId;
    @Expose
    private Integer ChipNum;
    @Expose
    private String Star;
    @Expose
    private Integer ChipAmount;
    @Expose
    private String Coins;
    private transient String cardName;
    
    public Integer getCardId() {
        return this.CardId;
    }
    
    public void setCardId(final Integer CardId) {
        this.CardId = CardId;
    }
    
    public Integer getChipNum() {
        return this.ChipNum;
    }
    
    public void setChipNum(final Integer ChipNum) {
        this.ChipNum = ChipNum;
    }
    
    public String getStar() {
        return this.Star;
    }
    
    public void setStar(final String Star) {
        this.Star = Star;
    }
    
    public Integer getChipAmount() {
        return this.ChipAmount;
    }
    
    public void setChipAmount(final Integer ChipAmount) {
        this.ChipAmount = ChipAmount;
    }
    
    public String getCoins() {
        return this.Coins;
    }
    
    public void setCoins(final String Coins) {
        this.Coins = Coins;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
    
    public String getCardName() {
        return this.cardName;
    }
    
    public void setCardName(final String cardName) {
        this.cardName = cardName;
    }
}
