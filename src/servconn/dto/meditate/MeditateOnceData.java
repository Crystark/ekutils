// 
// EkUtils 
// 

package servconn.dto.meditate;

import org.apache.commons.lang3.builder.ToStringBuilder;
import java.util.ArrayList;
import com.google.gson.annotations.Expose;
import java.util.List;

public class MeditateOnceData
{
    @Expose
    private List<Integer> NpcList;
    @Expose
    private Integer Coins;
    @Expose
    private MeditateAwardItem AwardItem;
    
    public MeditateOnceData() {
        this.NpcList = new ArrayList<Integer>();
    }
    
    public List<Integer> getNpcList() {
        return this.NpcList;
    }
    
    public void setNpcList(final List<Integer> NpcList) {
        this.NpcList = NpcList;
    }
    
    public Integer getCoins() {
        return this.Coins;
    }
    
    public void setCoins(final Integer Coins) {
        this.Coins = Coins;
    }
    
    public MeditateAwardItem getAwardItem() {
        return this.AwardItem;
    }
    
    public void setAwardItem(final MeditateAwardItem AwardItem) {
        this.AwardItem = AwardItem;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
