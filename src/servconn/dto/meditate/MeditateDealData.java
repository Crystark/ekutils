// 
// EkUtils 
// 

package servconn.dto.meditate;

import org.apache.commons.lang3.builder.ToStringBuilder;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;

public class MeditateDealData
{
    @Expose
    private Integer Coins;
    @Expose
    private List<MeditateDealSell> Sells;
    @Expose
    private List<MeditateDealReward> Rewards;
    
    public MeditateDealData() {
        this.Sells = new ArrayList<MeditateDealSell>();
        this.Rewards = new ArrayList<MeditateDealReward>();
    }
    
    public Integer getCoins() {
        return this.Coins;
    }
    
    public void setCoins(final Integer Coins) {
        this.Coins = Coins;
    }
    
    public List<MeditateDealSell> getSells() {
        return this.Sells;
    }
    
    public void setSells(final List<MeditateDealSell> Sells) {
        this.Sells = Sells;
    }
    
    public List<MeditateDealReward> getRewards() {
        return this.Rewards;
    }
    
    public void setRewards(final List<MeditateDealReward> Rewards) {
        this.Rewards = Rewards;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
