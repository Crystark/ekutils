// 
// EkUtils 
// 

package servconn.dto.hydralist;

import org.apache.commons.lang3.builder.ToStringBuilder;
import java.util.ArrayList;
import com.google.gson.annotations.Expose;
import java.util.List;

public class Data
{
    @Expose
    private List<HydraList> hydraList;
    @Expose
    private Integer userPointRank;
    @Expose
    private Integer userPoints;
    
    public Data() {
        this.hydraList = new ArrayList<HydraList>();
    }
    
    public List<HydraList> getHydraList() {
        return this.hydraList;
    }
    
    public void setHydraList(final List<HydraList> hydraList) {
        this.hydraList = hydraList;
    }
    
    public Integer getUserPointRank() {
        return this.userPointRank;
    }
    
    public void setUserPointRank(final Integer userPointRank) {
        this.userPointRank = userPointRank;
    }
    
    public Integer getUserPoints() {
        return this.userPoints;
    }
    
    public void setUserPoints(final Integer userPoints) {
        this.userPoints = userPoints;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
