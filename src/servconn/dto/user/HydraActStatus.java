// 
// EkUtils 
// 

package servconn.dto.user;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.google.gson.annotations.Expose;

public class HydraActStatus
{
    @Expose
    private Integer leftTime;
    @Expose
    private Integer isOpen;
    
    public Integer getLeftTime() {
        return this.leftTime;
    }
    
    public void setLeftTime(final Integer leftTime) {
        this.leftTime = leftTime;
    }
    
    public Integer getIsOpen() {
        return this.isOpen;
    }
    
    public void setIsOpen(final Integer isOpen) {
        this.isOpen = isOpen;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
