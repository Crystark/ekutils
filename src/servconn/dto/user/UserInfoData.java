// 
// EkUtils 
// 

package servconn.dto.user;

import org.apache.commons.lang3.builder.ToStringBuilder;
import servconn.dto.common.Version;
import com.google.gson.annotations.SerializedName;
import com.google.gson.annotations.Expose;

public class UserInfoData
{
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private UserInfo userInfo;
    @Expose
    private Version version;
    
    public Integer getStatus() {
        return this.status;
    }
    
    public void setStatus(final Integer status) {
        this.status = status;
    }
    
    public UserInfo getUserInfo() {
        return this.userInfo;
    }
    
    public void setUserInfo(final UserInfo userInfo) {
        this.userInfo = userInfo;
    }
    
    public Version getVersion() {
        return this.version;
    }
    
    public void setVersion(final Version version) {
        this.version = version;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
