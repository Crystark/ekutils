// 
// EkUtils 
// 

package servconn.dto.mapstage;

import java.util.ArrayList;
import servconn.dto.common.Version;
import java.util.List;
import com.google.gson.annotations.Expose;

public class MapStageData
{
    @Expose
    private Integer status;
    @Expose
    private List<MapStage> data;
    @Expose
    private Version version;
    
    public MapStageData() {
        this.data = new ArrayList<MapStage>();
    }
    
    public Integer getStatus() {
        return this.status;
    }
    
    public void setStatus(final Integer status) {
        this.status = status;
    }
    
    public List<MapStage> getData() {
        return this.data;
    }
    
    public void setData(final List<MapStage> data) {
        this.data = data;
    }
    
    public Version getVersion() {
        return this.version;
    }
    
    public void setVersion(final Version version) {
        this.version = version;
    }
}
