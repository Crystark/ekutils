// 
// EkUtils 
// 

package servconn.dto.mapstage;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.google.gson.annotations.Expose;

public class Level
{
    @Expose
    private String MapStageDetailId;
    @Expose
    private String Level;
    @Expose
    private String CardList;
    @Expose
    private String RuneList;
    @Expose
    private String HeroLevel;
    @Expose
    private String AchievementId;
    @Expose
    private String AchievementId2;
    @Expose
    private String EnergyExpend;
    @Expose
    private String BonusWin;
    @Expose
    private String FirstBonusWin;
    @Expose
    private String BonusLose;
    @Expose
    private String AddedBonus;
    @Expose
    private String EnergyExplore;
    @Expose
    private String BonusExplore;
    @Expose
    private String Hint;
    @Expose
    private String AchievementText;
    
    public String getMapStageDetailId() {
        return this.MapStageDetailId;
    }
    
    public void setMapStageDetailId(final String MapStageDetailId) {
        this.MapStageDetailId = MapStageDetailId;
    }
    
    public String getLevel() {
        return this.Level;
    }
    
    public void setLevel(final String Level) {
        this.Level = Level;
    }
    
    public String getCardList() {
        return this.CardList;
    }
    
    public void setCardList(final String CardList) {
        this.CardList = CardList;
    }
    
    public String getRuneList() {
        return this.RuneList;
    }
    
    public void setRuneList(final String RuneList) {
        this.RuneList = RuneList;
    }
    
    public String getHeroLevel() {
        return this.HeroLevel;
    }
    
    public void setHeroLevel(final String HeroLevel) {
        this.HeroLevel = HeroLevel;
    }
    
    public String getAchievementId() {
        return this.AchievementId;
    }
    
    public void setAchievementId(final String AchievementId) {
        this.AchievementId = AchievementId;
    }
    
    public String getAchievementId2() {
        return this.AchievementId2;
    }
    
    public void setAchievementId2(final String AchievementId2) {
        this.AchievementId2 = AchievementId2;
    }
    
    public String getEnergyExpend() {
        return this.EnergyExpend;
    }
    
    public void setEnergyExpend(final String EnergyExpend) {
        this.EnergyExpend = EnergyExpend;
    }
    
    public String getBonusWin() {
        return this.BonusWin;
    }
    
    public void setBonusWin(final String BonusWin) {
        this.BonusWin = BonusWin;
    }
    
    public String getFirstBonusWin() {
        return this.FirstBonusWin;
    }
    
    public void setFirstBonusWin(final String FirstBonusWin) {
        this.FirstBonusWin = FirstBonusWin;
    }
    
    public String getBonusLose() {
        return this.BonusLose;
    }
    
    public void setBonusLose(final String BonusLose) {
        this.BonusLose = BonusLose;
    }
    
    public String getAddedBonus() {
        return this.AddedBonus;
    }
    
    public void setAddedBonus(final String AddedBonus) {
        this.AddedBonus = AddedBonus;
    }
    
    public String getEnergyExplore() {
        return this.EnergyExplore;
    }
    
    public void setEnergyExplore(final String EnergyExplore) {
        this.EnergyExplore = EnergyExplore;
    }
    
    public String getBonusExplore() {
        return this.BonusExplore;
    }
    
    public void setBonusExplore(final String BonusExplore) {
        this.BonusExplore = BonusExplore;
    }
    
    public String getHint() {
        return this.Hint;
    }
    
    public void setHint(final String Hint) {
        this.Hint = Hint;
    }
    
    public String getAchievementText() {
        return this.AchievementText;
    }
    
    public void setAchievementText(final String AchievementText) {
        this.AchievementText = AchievementText;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
