// 
// EkUtils 
// 

package servconn.dto.usercard;

import com.google.gson.annotations.Expose;

public class UserCard
{
    @Expose
    private String UserCardId;
    @Expose
    private Integer Uid;
    @Expose
    private Integer CardId;
    @Expose
    private Integer Level;
    @Expose
    private String Exp;
    @Expose
    private Integer Evolution;
    @Expose
    private Integer WashTime;
    @Expose
    private Integer SkillNew;
    @Expose
    private Object TempSkills;
    
    public String getUserCardId() {
        return this.UserCardId;
    }
    
    public void setUserCardId(final String UserCardId) {
        this.UserCardId = UserCardId;
    }
    
    public Integer getUid() {
        return this.Uid;
    }
    
    public void setUid(final Integer Uid) {
        this.Uid = Uid;
    }
    
    public Integer getCardId() {
        return this.CardId;
    }
    
    public void setCardId(final Integer CardId) {
        this.CardId = CardId;
    }
    
    public Integer getLevel() {
        return this.Level;
    }
    
    public void setLevel(final Integer Level) {
        this.Level = Level;
    }
    
    public String getExp() {
        return this.Exp;
    }
    
    public void setExp(final String Exp) {
        this.Exp = Exp;
    }
    
    public Integer getEvolution() {
        return this.Evolution;
    }
    
    public void setEvolution(final Integer Evolution) {
        this.Evolution = Evolution;
    }
    
    public Integer getWashTime() {
        return this.WashTime;
    }
    
    public void setWashTime(final Integer WashTime) {
        this.WashTime = WashTime;
    }
    
    public Integer getSkillNew() {
        return this.SkillNew;
    }
    
    public void setSkillNew(final Integer SkillNew) {
        this.SkillNew = SkillNew;
    }
    
    public Object getTempSkills() {
        return this.TempSkills;
    }
    
    public void setTempSkills(final Object TempSkills) {
        this.TempSkills = TempSkills;
    }
}
