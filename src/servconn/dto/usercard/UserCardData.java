// 
// EkUtils 
// 

package servconn.dto.usercard;

import servconn.dto.common.Version;
import com.google.gson.annotations.Expose;

public class UserCardData
{
    @Expose
    private Integer status;
    @Expose
    private Data data;
    @Expose
    private Version version;
    
    public Integer getStatus() {
        return this.status;
    }
    
    public void setStatus(final Integer status) {
        this.status = status;
    }
    
    public Data getData() {
        return this.data;
    }
    
    public void setData(final Data data) {
        this.data = data;
    }
    
    public Version getVersion() {
        return this.version;
    }
    
    public void setVersion(final Version version) {
        this.version = version;
    }
}
