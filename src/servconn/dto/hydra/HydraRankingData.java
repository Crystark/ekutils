// 
// EkUtils 
// 

package servconn.dto.hydra;

import org.apache.commons.lang3.builder.ToStringBuilder;
import java.util.ArrayList;
import com.google.gson.annotations.Expose;
import java.util.List;

public class HydraRankingData
{
    @Expose
    private List<PersonHydra> PersonHydra;
    @Expose
    private List<LegionHydra> LegionHydra;
    
    public HydraRankingData() {
        this.PersonHydra = new ArrayList<PersonHydra>();
        this.LegionHydra = new ArrayList<LegionHydra>();
    }
    
    public List<PersonHydra> getPersonHydra() {
        return this.PersonHydra;
    }
    
    public void setPersonHydra(final List<PersonHydra> PersonHydra) {
        this.PersonHydra = PersonHydra;
    }
    
    public List<LegionHydra> getLegionHydra() {
        return this.LegionHydra;
    }
    
    public void setLegionHydra(final List<LegionHydra> LegionHydra) {
        this.LegionHydra = LegionHydra;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
