// 
// EkUtils 
// 

package servconn.dto.hydra;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.google.gson.annotations.Expose;

public class HydraPointAward
{
    @Expose
    private Integer kill;
    @Expose
    private Integer finder;
    @Expose
    private Integer mvp;
    @Expose
    private Integer lastAttack;
    @Expose
    private Integer card;
    @Expose
    private Integer totalPoint;
    
    public Integer getKill() {
        return this.kill;
    }
    
    public void setKill(final Integer kill) {
        this.kill = kill;
    }
    
    public Integer getFinder() {
        return this.finder;
    }
    
    public void setFinder(final Integer finder) {
        this.finder = finder;
    }
    
    public Integer getMvp() {
        return this.mvp;
    }
    
    public void setMvp(final Integer mvp) {
        this.mvp = mvp;
    }
    
    public Integer getLastAttack() {
        return this.lastAttack;
    }
    
    public void setLastAttack(final Integer lastAttack) {
        this.lastAttack = lastAttack;
    }
    
    public Integer getCard() {
        return this.card;
    }
    
    public void setCard(final Integer card) {
        this.card = card;
    }
    
    public Integer getTotalPoint() {
        return this.totalPoint;
    }
    
    public void setTotalPoint(final Integer totalPoint) {
        this.totalPoint = totalPoint;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
