// 
// EkUtils 
// 

package servconn.dto.hydra;

import org.apache.commons.lang3.builder.ToStringBuilder;
import java.util.ArrayList;
import com.google.gson.annotations.Expose;
import java.util.List;

public class FriendPointsData
{
    @Expose
    private List<FriendPointsList> friendPointsList;
    
    public FriendPointsData() {
        this.friendPointsList = new ArrayList<FriendPointsList>();
    }
    
    public List<FriendPointsList> getFriendPointsList() {
        return this.friendPointsList;
    }
    
    public void setFriendPointsList(final List<FriendPointsList> friendPointsList) {
        this.friendPointsList = friendPointsList;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
