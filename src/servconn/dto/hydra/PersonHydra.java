// 
// EkUtils 
// 

package servconn.dto.hydra;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.google.gson.annotations.Expose;

public class PersonHydra
{
    @Expose
    private String Name;
    @Expose
    private String Sex;
    @Expose
    private String HeadId;
    @Expose
    private String Level;
    @Expose
    private String Descr1;
    @Expose
    private Integer rank;
    @Expose
    private Integer Belong;
    
    public String getName() {
        return this.Name;
    }
    
    public void setName(final String Name) {
        this.Name = Name;
    }
    
    public String getSex() {
        return this.Sex;
    }
    
    public void setSex(final String Sex) {
        this.Sex = Sex;
    }
    
    public String getHeadId() {
        return this.HeadId;
    }
    
    public void setHeadId(final String HeadId) {
        this.HeadId = HeadId;
    }
    
    public String getLevel() {
        return this.Level;
    }
    
    public void setLevel(final String Level) {
        this.Level = Level;
    }
    
    public String getDescr1() {
        return this.Descr1;
    }
    
    public void setDescr1(final String Descr1) {
        this.Descr1 = Descr1;
    }
    
    public Integer getRank() {
        return this.rank;
    }
    
    public void setRank(final Integer rank) {
        this.rank = rank;
    }
    
    public Integer getBelong() {
        return this.Belong;
    }
    
    public void setBelong(final Integer Belong) {
        this.Belong = Belong;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
