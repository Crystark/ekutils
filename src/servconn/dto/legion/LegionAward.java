// 
// EkUtils 
// 

package servconn.dto.legion;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.google.gson.annotations.Expose;

public class LegionAward
{
    @Expose
    private Integer CardId;
    @Expose
    private Integer Status;
    @Expose
    private String Text;
    @Expose
    private Integer RankLevel;
    @Expose
    private Integer Id;
    
    public Integer getCardId() {
        return this.CardId;
    }
    
    public void setCardId(final Integer CardId) {
        this.CardId = CardId;
    }
    
    public Integer getStatus() {
        return this.Status;
    }
    
    public void setStatus(final Integer Status) {
        this.Status = Status;
    }
    
    public String getText() {
        return this.Text;
    }
    
    public void setText(final String Text) {
        this.Text = Text;
    }
    
    public Integer getRankLevel() {
        return this.RankLevel;
    }
    
    public void setRankLevel(final Integer RankLevel) {
        this.RankLevel = RankLevel;
    }
    
    public Integer getId() {
        return this.Id;
    }
    
    public void setId(final Integer Id) {
        this.Id = Id;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
