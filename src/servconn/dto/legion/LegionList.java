// 
// EkUtils 
// 

package servconn.dto.legion;

import org.apache.commons.lang3.builder.ToStringBuilder;
import servconn.dto.common.Version;
import com.google.gson.annotations.Expose;

public class LegionList
{
    @Expose
    private Integer status;
    @Expose
    private LegionListData data;
    @Expose
    private Version version;
    
    public Integer getStatus() {
        return this.status;
    }
    
    public void setStatus(final Integer status) {
        this.status = status;
    }
    
    public LegionListData getData() {
        return this.data;
    }
    
    public void setData(final LegionListData data) {
        this.data = data;
    }
    
    public Version getVersion() {
        return this.version;
    }
    
    public void setVersion(final Version version) {
        this.version = version;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
