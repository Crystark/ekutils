//
// EkUtils
//

package servconn.client;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import servconn.dto.login.GuestLoginDto;
import servconn.dto.login.Player;
import servconn.dto.login.ServerLoginUserInfo;

public abstract class BaseClient {
	private static final Logger	logger		= LogManager.getLogger();
	protected static Gson		GSON		= new GsonBuilder().setPrettyPrinting().create();
	private static String		secretKey	= "nj3dy&hf3h#jui$5lyf!s54";

	private OkHttpClient		client;
	protected LoginData			loginData;
	protected Integer			authSerial;
	private String				CommonAA;
	private String				CommonBB;
	private String				CommonCC;
	private String				CommonDD;
	private String				CommonEE;
	private String				CommonFF;

	BaseClient() {
		this.authSerial = 0;
		this.CommonAA = "";
		this.CommonBB = "";
		this.CommonCC = "";
		this.CommonDD = "";
		this.CommonEE = "";
		this.CommonFF = "";
		this.client = ConnectionManager.getInstance().getClient();
		this.loginData = new LoginData();
		this.initializeHashKeys();
	}

	private void initializeHashKeys() {
		try {
			this.CommonAA = this.getMd5("320").substring(25);
			this.CommonBB = "package_AchievementTex_1";
			this.CommonCC = this.getMd5("127").substring(23);
			this.CommonDD = "keytest1234";
			this.CommonEE = this.getMd5("1").substring(20);
			this.CommonFF = this.getMd5("1").substring(20);
		}
		catch (NoSuchAlgorithmException ex) {}
		catch (UnsupportedEncodingException ex2) {}
	}

	protected Response doGetRequest(final String url) throws IOException {
		final Request request = new Request.Builder().url(url).build();
		final Response response = this.client.newCall(request).execute();
		return response;
	}

	protected Response doPostRequest(final String url, final RequestBody formBody) throws IOException {
		final Request request = new Request.Builder().url(url).post(formBody).build();
		final Response response = this.client.newCall(request).execute();
		if (!response.isSuccessful()) {
			throw new IOException("Unexpected code " + response);
		}
		return response;
	}

	protected void doMpLogin() throws IOException {
		final ServerLoginUserInfo userInfo = this.loginData.getServerLogin().getData().getUinfo();
		final RequestBody formBody =
			new FormEncodingBuilder().add("plat", "pwe").add("uin", this.loginData.getPlayer().getUin()).add("nickName", this.loginData.getPlayer().getUin()).add("Devicetoken", this.loginData.getPlayer().getDeviceId()).add("userType", "2").add("MUid", userInfo.getMUid().toString()).add("ppsign", userInfo.getPpsign()).add("sign", userInfo.getSign()).add("nick", userInfo.getNick()).add("time", userInfo.getTime().toString()).add("Udid", "00-C0-7B-9F-0A-01").add("Origin", "IOS_PW").build();
		final Response r = this.doPostRequest(this.loginData.getServerUrl() + "/login.php?do=mpLogin", formBody);
		final String json = r.body().string();
		final JsonObject joData = this.getDataFromJson(json);
		this.authSerial = joData.get("AuthSerial").getAsInt();
		this.loginData.getPlayer().setAuthType(joData.get("AuthType").getAsInt());
		this.loginData.getPlayer().setAuthSerial(this.authSerial);
		BaseClient.logger.debug(this.loginData);
	}

	protected Player getPlayerFromConnectionData(final String json) {
		final GuestLoginDto guestLogin = BaseClient.GSON.fromJson(json, GuestLoginDto.class);
		if (guestLogin.getResult()) {
			final String[] loginStatus = guestLogin.getLoginstatus().split(":");
			final Player p = new Player();
			p.setUin(loginStatus[1]);
			p.setDeviceToken(loginStatus[3]);
			return p;
		}
		final String msg = guestLogin.getMsg();
		BaseClient.logger.fatal(msg);
		throw new IllegalArgumentException(msg);
	}

	protected abstract void login() throws IOException;

	protected abstract void login(final String p0) throws IOException;

	protected JsonObject getDataFromJson(final String json) {
		final JsonObject jo = new JsonParser().parse(json).getAsJsonObject();
		final JsonObject data = jo.getAsJsonObject("data");
		return data;
	}

	public LoginData getLoginData() {
		return this.loginData;
	}

	protected String getServiceUrl(final String url) {
		final StringBuilder sb = new StringBuilder();
		sb.append(this.loginData.getServerUrl());
		sb.append(url);
		sb.append("&phpl=EN");
		sb.append("&pvc=1.7.4");
		sb.append("&phpp=ANDROID_ARC");
		sb.append("&phps=").append(this.authSerial);
		sb.append("&phpk=").append(this.Md5Sum());
		++this.authSerial;
		return sb.toString();
	}

	private String Md5Sum() {
		if (this.loginData.getUserInfo() == null) {
			return "0";
		}
		final String UID = this.loginData.getUserInfo().getUid();
		final Integer authType = this.loginData.getPlayer().getAuthType();
		final StringBuilder sb = new StringBuilder();
		sb.append(UID);
		sb.append(authType);
		sb.append(BaseClient.secretKey);
		sb.append(this.authSerial);
		switch (authType) {
			case 1: {
				sb.append(this.CommonAA);
				break;
			}
			case 2: {
				sb.append(this.CommonBB);
				break;
			}
			case 3: {
				sb.append(this.CommonCC);
				break;
			}
			case 4: {
				sb.append(this.CommonDD);
				break;
			}
			case 5: {
				sb.append(this.CommonEE);
				break;
			}
			case 6: {
				sb.append(this.CommonFF);
				break;
			}
		}
		String result;
		try {
			result = this.getMd5(sb.toString());
		}
		catch (NoSuchAlgorithmException | UnsupportedEncodingException ex2) {
			result = "";
		}
		return result;
	}

	private String getMd5(final String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		final byte[] bytesOfMessage = text.getBytes("UTF-8");
		final MessageDigest md = MessageDigest.getInstance("MD5");
		final byte[] thedigest = md.digest(bytesOfMessage);
		final BigInteger bigInt = new BigInteger(1, thedigest);
		return String.format("%1$032x", bigInt);
	}
}
