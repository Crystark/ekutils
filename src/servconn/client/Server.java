package servconn.client;

import static servconn.client.Game.ek;
import static servconn.client.Game.mr;

public enum Server {
	equilibrium(ek, "s1.ek"),
	aeon(ek, "s2.ek"),
	ethereal(ek, "s3.ek"),
	ascension(ek, "s4.ek"),
	skorn(ek, "s1.ekru"),
	apollo(mr, "s1.ekbb");

	private final String	strValue;
	public final Game		game;

	private Server(Game game, String strValue) {
		this.strValue = strValue;
		this.game = game;
	}

	@Override
	public String toString() {
		return this.strValue;
	}
}
