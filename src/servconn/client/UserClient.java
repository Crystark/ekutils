package servconn.client;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import servconn.dto.arena.Competitor;
import servconn.dto.arena.CompetitorData;
import servconn.dto.battle.BattleData;
import servconn.dto.buy.BuyingData;
import servconn.dto.buy.BuyingResult;
import servconn.dto.card.CardCollect;
import servconn.dto.card.CardFragment;
import servconn.dto.card.Collection;
import servconn.dto.card.Fragment;
import servconn.dto.ew.EwBoss;
import servconn.dto.ew.EwBossData;
import servconn.dto.foh.Tavern;
import servconn.dto.foh.TavernData;
import servconn.dto.friend.Friend;
import servconn.dto.friend.FriendData;
import servconn.dto.hydra.FriendPoints;
import servconn.dto.hydra.FriendPointsList;
import servconn.dto.hydra.HydraClaim;
import servconn.dto.hydra.HydraPointAward;
import servconn.dto.hydra.HydraRanking;
import servconn.dto.hydra.HydraRankingData;
import servconn.dto.hydralist.HydraData;
import servconn.dto.hydralist.HydraList;
import servconn.dto.legion.LegionInfo;
import servconn.dto.legion.LegionList;
import servconn.dto.mapstage.MapStageData;
import servconn.dto.mapstage.UserMapStageDetail;
import servconn.dto.maze.MazeAward;
import servconn.dto.maze.MazeExtData;
import servconn.dto.maze.MazeFloorInfo;
import servconn.dto.maze.MazeFloorInfoData;
import servconn.dto.maze.MazeInfo;
import servconn.dto.maze.MazeInfoData;
import servconn.dto.meditate.MeditateAwardItem;
import servconn.dto.meditate.MeditateDeal;
import servconn.dto.meditate.MeditateDealData;
import servconn.dto.meditate.MeditateMultiple;
import servconn.dto.meditate.MeditateOnce;
import servconn.dto.user.UserInfo;
import servconn.dto.user.UserInfoData;
import servconn.dto.usercard.UserCard;
import servconn.dto.usercard.UserCardData;
import servconn.dto.userdeck.UserDeck;
import servconn.dto.userdeck.UserDeckData;

public abstract class UserClient extends BaseClient {
	private static final Logger	logger	= LogManager.getLogger();
	private static int			VALIDSTATUS;
	private static String		BATTLE_WON;

	static {
		UserClient.VALIDSTATUS = 1;
		UserClient.BATTLE_WON = "1";
	}

	protected UserClient() {}

	UserClient(final String email, final String password) {
		this.loginData.setEmail(email);
		this.loginData.setPassword(password);
	}

	public String getFriendsAsJson() throws IOException {
		this.login();
		final Response r = this.doGetRequest(this.loginData.getServerUrl() + "/friend.php?do=GetFriends&phpl=EN");
		final String body = r.body().string();
		UserClient.logger.debug(body);
		return body;
	}

	public List<Friend> getFriends() throws IOException {
		final String json = this.getFriendsAsJson();
		final FriendData friendData = UserClient.GSON.fromJson(json, FriendData.class);
		return friendData.getData().getFriends();
	}

	public String getUserInfoAsJson() throws IOException {
		this.login();
		final Response r = this.doGetRequest(this.getServiceUrl(EkUrl.USER_INFO));
		final String json = r.body().string();
		UserClient.logger.debug(json);
		return json;
	}

	public UserInfo getUserInfo() throws IOException {
		final String json = this.getUserInfoAsJson();
		final UserInfoData userInfoData = UserClient.GSON.fromJson(json, UserInfoData.class);
		UserClient.logger.debug(userInfoData.getUserInfo());
		return userInfoData.getUserInfo();
	}

	public String getCompetitorsAsJson() throws IOException {
		this.login();
		final Response r = this.doGetRequest(this.loginData.getServerUrl() + "/arena.php?do=GetCompetitors&phpl=EN");
		final String json = r.body().string();
		UserClient.logger.debug(json);
		return json;
	}

	public String getArenaRankCompetitorsAsJson() throws IOException {
		this.login();
		final Response r = this.doGetRequest(this.loginData.getServerUrl() + "/arena.php?do=GetRankCompetitors&phpl=EN");
		final String json = r.body().string();
		UserClient.logger.debug(json);
		return json;
	}

	public String getRankUsersAsJson(final Integer start, final Integer amount) throws IOException {
		this.login();
		final RequestBody formBody = new FormEncodingBuilder().add("Amount", amount.toString()).add("StartRank", start.toString()).build();
		final Response r = this.doPostRequest(this.loginData.getServerUrl() + "/arena.php?do=GetRankUsers&phpl=EN", formBody);
		final String json = r.body().string();
		UserClient.logger.debug(json);
		return json;
	}

	public String getHydraFriendPointsAsJson() throws IOException {
		this.login();
		final Response r = this.doGetRequest(this.getServiceUrl(EkUrl.GET_GVE_FRIEND_CONTRIBUTION));
		final String json = r.body().string();
		UserClient.logger.debug(json);
		this.parseJsonResponse(json);
		return json;
	}

	public List<FriendPointsList> getHydraPoints() throws IOException {
		final String json = this.getHydraFriendPointsAsJson();
		final FriendPoints friendPointsData = UserClient.GSON.fromJson(json, FriendPoints.class);
		return friendPointsData.getData().getFriendPointsList();
	}

	public String getUserHydraListAsJson() throws IOException {
		this.login();
		final Response r = this.doGetRequest(this.getServiceUrl(EkUrl.GET_GVE_BOSS_LIST));
		final String json = r.body().string();
		UserClient.logger.debug(json);
		this.parseJsonResponse(json);
		return json;
	}

	public List<HydraList> getUserHydraList() throws IOException {
		final String json = this.getUserHydraListAsJson();
		final HydraData hydraData = UserClient.GSON.fromJson(json, HydraData.class);
		return hydraData.getData().getHydraList();
	}

	public List<Competitor> getRankUsers(final Integer start, final Integer amount) throws IOException {
		final String json = this.getRankUsersAsJson(start, amount);
		final CompetitorData competitorData = UserClient.GSON.fromJson(json, CompetitorData.class);
		return competitorData.getData().getCompetitors();
	}

	public String doFriendlyMatch(final String userId) throws IOException {
		this.login();
		final RequestBody formBody = new FormEncodingBuilder().add("isManual", "0").add("Competitor", userId).add("NoChip", "1").build();
		final Response response = this.doPostRequest(this.loginData.getServerUrl() + "/arena.php?do=FreeFight&phpl=EN", formBody);
		final String json = response.body().string();
		UserClient.logger.debug(json);
		return json;
	}

	public BattleData getFriendlyBattleData(final String userId) throws IOException {
		final String json = this.doFriendlyMatch(userId);
		final BattleData battleData = UserClient.GSON.fromJson(json, BattleData.class);
		return battleData;
	}

	public String getKwStore() throws IOException {
		this.login();
		final Response r = this.doGetRequest(this.loginData.getServerUrl() + "/forceshop.php?do=GetExchangeGoods&phpl=EN");
		final String json = r.body().string();
		UserClient.logger.debug(json);
		return json;
	}

	public String getKwRanking() throws IOException {
		this.login();
		final RequestBody formBody = new FormEncodingBuilder().add("NewVersion", "1").build();
		final Response response = this.doPostRequest(this.loginData.getServerUrl() + "/forcefight.php?do=GetForceStatus&phpl=EN", formBody);
		final String json = response.body().string();
		UserClient.logger.debug(json);
		return json;
	}

	public String getUserCardsAsJson() throws IOException {
		this.login();
		final Response r = this.doGetRequest(this.getServiceUrl(EkUrl.CARD_USERCARDS));
		final String body = r.body().string();
		UserClient.logger.debug(body);
		return body;
	}

	public List<UserCard> getUserCards() throws IOException {
		final String json = this.getUserCardsAsJson();
		final UserCardData userCardData = UserClient.GSON.fromJson(json, UserCardData.class);
		return userCardData.getData().getCards();
	}

	public String getUserDecksAsJson() throws IOException {
		this.login();
		final Response r = this.doGetRequest(this.loginData.getServerUrl() + "/card.php?do=GetCardGroup&phpl=EN");
		final String body = r.body().string();
		UserClient.logger.debug(body);
		return body;
	}

	public UserDeckData getUserDeckData() throws IOException {
		final String json = this.getUserDecksAsJson();
		final UserDeck userCardData = UserClient.GSON.fromJson(json, UserDeck.class);
		return userCardData.getData();
	}

	public String editAvatar(final String avatarId) throws IOException {
		this.login();
		final RequestBody formBody = new FormEncodingBuilder().add("Avatar", avatarId).build();
		final Response response = this.doPostRequest(this.loginData.getServerUrl() + "/user.php?do=EditAvatar&phpl=EN", formBody);
		final String json = response.body().string();
		UserClient.logger.debug(json);
		return json;
	}

	public String setActiveDeckId(final String deckId) throws IOException {
		this.login();
		final RequestBody formBody = new FormEncodingBuilder().add("GroupId", deckId).build();
		final Response response = this.doPostRequest(this.loginData.getServerUrl() + "/card.php?do=SetDefalutGroup&phpl=EN", formBody);
		final String json = response.body().string();
		UserClient.logger.debug(json);
		return json;
	}

	public String showMazeInfo(final Integer mapStageId) throws IOException {
		this.login();
		final RequestBody formBody = new FormEncodingBuilder().add("MapStageId", mapStageId.toString()).build();
		final Response response = this.doPostRequest(this.loginData.getServerUrl() + "/maze.php?do=Show&phpl=EN&pvc=1.8.0", formBody);
		final String json = response.body().string();
		UserClient.logger.debug(json);
		return json;
	}

	public MazeInfoData showMazeInfoData(final Integer mapStageId) throws IOException {
		final String json = this.showMazeInfo(mapStageId);
		final MazeInfo mazeInfo = UserClient.GSON.fromJson(json, MazeInfo.class);
		return mazeInfo.getData();
	}

	public String showMazeFloorInfo(final Integer mapStageId, final Integer floor) throws IOException {
		this.login();
		final RequestBody formBody = new FormEncodingBuilder().add("MapStageId", mapStageId.toString()).add("Layer", floor.toString()).build();
		final Response response = this.doPostRequest(this.loginData.getServerUrl() + "/maze.php?do=Info&phpl=EN&pvc=1.8.0", formBody);
		final String json = response.body().string();
		UserClient.logger.debug(json);
		this.parseJsonResponse(json);
		return json;
	}

	public MazeFloorInfoData showMazeFloorInfoData(final Integer mapStageId, final Integer floor) throws IOException {
		final String json = this.showMazeFloorInfo(mapStageId, floor);
		final MazeFloorInfo mazeFloorInfo = UserClient.GSON.fromJson(json, MazeFloorInfo.class);
		return mazeFloorInfo.getData();
	}

	@Override
	protected void login(final String server) throws IOException {}

	public String resetMaze(final Integer mapStageId) throws IOException {
		this.login();
		final RequestBody formBody = new FormEncodingBuilder().add("MapStageId", mapStageId.toString()).build();
		final Response response = this.doPostRequest(this.loginData.getServerUrl() + "/maze.php?do=Reset&phpl=EN&pvc=1.8.0", formBody);
		final String json = response.body().string();
		UserClient.logger.debug(json);
		return json;
	}

	private String startManualMazeBattle(final Integer mapStageId, final Integer layer, final Integer index) throws IOException {
		this.login();
		final RequestBody formBody = new FormEncodingBuilder().add("MapStageId", mapStageId.toString()).add("Layer", layer.toString()).add("manual", "1").add("ItemIndex", index.toString()).build();
		final Response response = this.doPostRequest(this.loginData.getServerUrl() + "/maze.php?do=Battle&phpl=EN&pvc=1.8.0", formBody);
		final String json = response.body().string();
		UserClient.logger.debug(json);
		final JsonResponse jr = this.parseJsonResponse(json);
		if (jr.validStatus) {
			final JsonObject joData = this.getDataFromJson(json);
			final String battleId = joData.get("BattleId").getAsString();
			UserClient.logger.debug("Battle ID: {}", battleId);
			return battleId;
		}
		throw new IllegalStateException(jr.message);
	}

	private JsonResponse parseJsonResponse(final String json) {
		final StringReader sr = new StringReader(json);
		final JsonReader jsonReader = new JsonReader(sr);
		jsonReader.setLenient(true);
		final JsonObject jo = new JsonParser().parse(jsonReader).getAsJsonObject();
		final int status = jo.get("status").getAsInt();
		final JsonResponse jr = new JsonResponse();
		jr.validStatus = (status == UserClient.VALIDSTATUS);
		final JsonElement msg = jo.get("message");
		if (msg != null) {
			jr.message = msg.getAsString();
		}
		if (!jr.validStatus) {
			throw new IllegalStateException(jr.message);
		}
		return jr;
	}

	private MazeExtData finishManualMazeBattle(final String battleId) throws IOException {
		this.login();
		final RequestBody formBody = new FormEncodingBuilder().add("battleid", battleId).add("manual", "0").add("MapStageDetailId", "").add("stage", "").build();
		final Response response = this.doPostRequest(this.loginData.getServerUrl() + "/maze.php?do=ManualBattle&phpl=EN&pvc=1.8.0", formBody);
		final String json = response.body().string();
		final JsonObject data = this.getDataFromJson(json);
		final JsonObject extData = data.getAsJsonObject("ExtData");
		final String win = data.get("Win").getAsString();
		if (UserClient.BATTLE_WON.equals(win)) {
			final MazeExtData mazeExt = UserClient.GSON.fromJson(extData, MazeExtData.class);
			UserClient.logger.debug(json);
			return mazeExt;
		}
		throw new IllegalStateException("You lost the battle, please select a powerful deck");
	}

	public MazeAward doMazeBattle(final Integer mapStageId, final Integer layer, final Integer index) throws IOException, IllegalStateException, InterruptedException {
		this.login();
		final String battleId = this.startManualMazeBattle(mapStageId, layer, index);
		Thread.sleep(200L);
		final MazeExtData mazeExtData = this.finishManualMazeBattle(battleId);
		Thread.sleep(200L);
		return mazeExtData.getAward();
	}

	public String buyGoodsAsJson(final String goodsId) throws IOException {
		this.login();
		final RequestBody formBody = new FormEncodingBuilder().add("version", "new").add("GoodsId", goodsId).build();
		final Response response = this.doPostRequest(this.getServiceUrl(EkUrl.SHOP_NEW_BUYGOODS), formBody);
		final String json = response.body().string();
		UserClient.logger.debug(json);
		this.parseJsonResponse(json);
		return json;
	}

	public BuyingResult buyGoods(final String goodsId) throws IOException {
		final String json = this.buyGoodsAsJson(goodsId);
		final BuyingData buyingData = UserClient.GSON.fromJson(json, BuyingData.class);
		return buyingData.getData();
	}

	public String claimGiftCodeAsJson(final String giftCode) throws IOException {
		this.login();
		final RequestBody formBody = new FormEncodingBuilder().add("code", giftCode).build();
		final Response response = this.doPostRequest(this.loginData.getServerUrl() + "/activity.php?do=GiftCodeReward&phpl=EN", formBody);
		final String json = response.body().string();
		UserClient.logger.debug(json);
		this.parseJsonResponse(json);
		return json;
	}

	public boolean claimGiftCode(final String giftCode) throws IOException {
		this.claimGiftCodeAsJson(giftCode);
		return true;
	}

	public String claimUserHydraAsJson(final Integer userHydraId) throws IOException {
		this.login();
		final RequestBody formBody = new FormEncodingBuilder().add("userHydraId", userHydraId.toString()).build();
		final Response response = this.doPostRequest(this.getServiceUrl(EkUrl.GET_GVE_RECEIVE_BOSS_REWARD), formBody);
		final String json = response.body().string();
		UserClient.logger.debug(json);
		this.parseJsonResponse(json);
		return json;
	}

	public HydraPointAward claimUserHydra(final Integer userHydraId) throws IOException {
		final String json = this.claimUserHydraAsJson(userHydraId);
		final HydraClaim hydraClaim = UserClient.GSON.fromJson(json, HydraClaim.class);
		return hydraClaim.getData().getPointAward();
	}

	public String getHydraRankingSelfAsJson() throws IOException {
		this.login();
		final Response r = this.doGetRequest(this.getServiceUrl(EkUrl.Get_ActionRankView_Data));
		final String json = r.body().string();
		UserClient.logger.debug(json);
		this.parseJsonResponse(json);
		return json;
	}

	public HydraRankingData getHydraRankingSelf() throws IOException {
		final String json = this.getHydraRankingSelfAsJson();
		final HydraRanking hydraRanking = UserClient.GSON.fromJson(json, HydraRanking.class);
		return hydraRanking.getData();
	}

	public String getUserHydraInfoAsJson(final Integer userHydraId) throws IOException {
		this.login();
		final RequestBody formBody = new FormEncodingBuilder().add("userHydraId", userHydraId.toString()).build();
		final Response response = this.doPostRequest(this.getServiceUrl(EkUrl.GET_GVE_BOSS_DETAIL), formBody);
		final String json = response.body().string();
		UserClient.logger.debug(json);
		this.parseJsonResponse(json);
		return json;
	}

	public String getLegionInfoAsJson(final String start, final String amount) throws IOException {
		this.login();
		final RequestBody formBody = new FormEncodingBuilder().add("Start", start).add("Amount", amount).build();
		final Response response = this.doPostRequest(this.loginData.getServerUrl() + "/legion.php?do=GetLegions&phpl=EN", formBody);
		final String json = response.body().string();
		UserClient.logger.debug(json);
		return json;
	}

	public List<LegionInfo> getLegionInfo(final String start, final String amount) throws IOException {
		final String json = this.getLegionInfoAsJson(start, amount);
		final LegionList legionList = UserClient.GSON.fromJson(json, LegionList.class);
		return legionList.getData().getLegionInfos();
	}

	public String doMeditateOnceAsJson() throws IOException {
		this.login();
		final RequestBody formBody = new FormEncodingBuilder().add("NpcId", "1").build();
		final Response response = this.doPostRequest(this.loginData.getServerUrl() + "/meditation.php?do=Npc&phpl=EN", formBody);
		final String json = response.body().string();
		UserClient.logger.debug(json);
		return json;
	}

	public MeditateAwardItem doMeditateOnce() throws IOException {
		final String json = this.doMeditateOnceAsJson();
		final MeditateOnce meditateOnce = UserClient.GSON.fromJson(json, MeditateOnce.class);
		return meditateOnce.getData().getAwardItem();
	}

	public String doMeditateMultipleAsJson() throws IOException {
		this.login();
		final Response r = this.doGetRequest(this.getServiceUrl(EkUrl.VIP_NPCONEKEY));
		final String json = r.body().string();
		UserClient.logger.debug(json);
		this.parseJsonResponse(json);
		return json;
	}

	public List<MeditateAwardItem> doMeditateMultiple() throws IOException {
		final String json = this.doMeditateMultipleAsJson();
		final MeditateMultiple meditateMultiple = UserClient.GSON.fromJson(json, MeditateMultiple.class);
		return meditateMultiple.getData().getAwardItem();
	}

	public String doMeditateDealAsJson() throws IOException {
		this.login();
		final Response r = this.doGetRequest(this.loginData.getServerUrl() + "/meditation.php?do=Deal&phpl=EN");
		final String json = r.body().string();
		UserClient.logger.debug(json);
		this.parseJsonResponse(json);
		return json;
	}

	public MeditateDealData doMeditateDeal() throws IOException {
		final String json = this.doMeditateDealAsJson();
		final MeditateDeal meditateDeal = UserClient.GSON.fromJson(json, MeditateDeal.class);
		return meditateDeal.getData();
	}

	public String getCardFragmentsAsJson() throws IOException {
		this.login();
		final Response r = this.doGetRequest(this.getServiceUrl(EkUrl.GET_CARD_CHIP_INFO));
		final String json = r.body().string();
		UserClient.logger.debug(json);
		this.parseJsonResponse(json);
		return json;
	}

	public List<CardFragment> getCardFragments() throws IOException {
		final String json = this.getCardFragmentsAsJson();
		final Fragment fragment = UserClient.GSON.fromJson(json, Fragment.class);
		return fragment.getData().getCardChips();
	}

	public String getEwBossInfoAsJson(final String bossId) throws IOException {
		this.login();
		final RequestBody formBody = new FormEncodingBuilder().add("bossNum", bossId).add("isNew", "1").build();
		final Response response = this.doPostRequest(this.getServiceUrl(EkUrl.SPRITEWAR_GETBOSSINFO), formBody);
		final String json = response.body().string();
		UserClient.logger.debug(json);
		this.parseJsonResponse(json);
		return json;
	}

	public EwBoss getEwBossInfo(final String bossId) throws IOException {
		final String json = this.getEwBossInfoAsJson(bossId);
		final EwBossData ewBossData = UserClient.GSON.fromJson(json, EwBossData.class);
		return ewBossData.getData();
	}

	public String doLotteryInfoAsJson() throws IOException {
		this.login();
		final Response r = this.doGetRequest(this.getServiceUrl(EkUrl.FIELDOFHONOR_PUBRESULT));
		final String json = r.body().string();
		UserClient.logger.debug(json);
		this.parseJsonResponse(json);
		return json;
	}

	public String doFohInfoAsJson() throws IOException {
		this.login();
		final Response r = this.doGetRequest(this.getServiceUrl(EkUrl.FIELDOFHONOR_GETINFO));
		final String json = r.body().string();
		UserClient.logger.debug(json);
		this.parseJsonResponse(json);
		return json;
	}

	public String doLotteryAsJson() throws IOException {
		this.login();
		final RequestBody formBody = new FormEncodingBuilder().add("New", "1").build();
		final Response response = this.doPostRequest(this.getServiceUrl(EkUrl.FIELDOFHONOR_PUBDO), formBody);
		final String json = response.body().string();
		UserClient.logger.debug(json);
		this.parseJsonResponse(json);
		return json;
	}

	public TavernData doLottery() throws IOException {
		final String json = this.doLotteryAsJson();
		final Tavern tavern = UserClient.GSON.fromJson(json, Tavern.class);
		return tavern.getData();
	}

	public String doSellCardsRunes(final String cards, final String runes) throws IOException {
		this.login();
		final RequestBody formBody = new FormEncodingBuilder().add("Cards", cards).add("Runes", runes).build();
		final Response response = this.doPostRequest(this.getServiceUrl(EkUrl.CARD_SELL), formBody);
		final String json = response.body().string();
		UserClient.logger.debug(json);
		this.parseJsonResponse(json);
		return json;
	}

	public String doMazeFightInfo(final String mapStageDetailId) throws IOException {
		this.login();
		final RequestBody formBody = new FormEncodingBuilder().add("MapStageDetailId", mapStageDetailId).add("isManual", "1").build();
		final Response response = this.doPostRequest(this.getServiceUrl(EkUrl.MAP_STARTFIGHT_INFO), formBody);
		final String json = response.body().string();
		UserClient.logger.debug(json);
		final JsonResponse jr = this.parseJsonResponse(json);
		if (jr.validStatus) {
			final JsonObject joData = this.getDataFromJson(json);
			final String battleId = joData.get("BattleId").getAsString();
			UserClient.logger.debug("Battle ID: {}", battleId);
			return battleId;
		}
		return "";
	}

	public String doMazeFightStart(final String mapStageDetailId, final String battleId) throws IOException {
		this.login();
		final RequestBody formBody = new FormEncodingBuilder().add("MapStageDetailId", mapStageDetailId).add("battleid", battleId).add("stage", "").add("isManual", "0").build();
		final Response response = this.doPostRequest(this.getServiceUrl(EkUrl.MAP_STARTFIGHT_MANUAL), formBody);
		final String json = response.body().string();
		UserClient.logger.debug(json);
		this.parseJsonResponse(json);
		return json;
	}

	public List<UserMapStageDetail> getUserMapStages() throws IOException {
		this.login();
		final Response r = this.doGetRequest(this.getServiceUrl(EkUrl.MAP_USERMAPSTAGES));
		final String json = r.body().string();
		UserClient.logger.debug(json);
		this.parseJsonResponse(json);
		final List<UserMapStageDetail> userMapStages = new ArrayList<UserMapStageDetail>();
		final JsonObject data = this.getDataFromJson(json);
		final Set<Map.Entry<String, JsonElement>> entrySet = data.entrySet();
		for (final Map.Entry<String, JsonElement> entry : entrySet) {
			final UserMapStageDetail userMapStageDetail = UserClient.GSON.fromJson(data.getAsJsonObject(entry.getKey()), UserMapStageDetail.class);
			userMapStages.add(userMapStageDetail);
		}
		return userMapStages;
	}

	private String getServerMapStagesAsJson() throws IOException {
		this.login();
		final Response response = this.doGetRequest(this.getServiceUrl(EkUrl.MAP_ALLMAPSTAGES));
		final String json = response.body().string();
		this.parseJsonResponse(json);
		return json;
	}

	public MapStageData getServerMapStages() throws IOException {
		final String json = this.getServerMapStagesAsJson();
		final MapStageData mapStageData = UserClient.GSON.fromJson(json, MapStageData.class);
		return mapStageData;
	}

	public String getCardCollectionAsJson() throws IOException {
		this.login();
		final Response r = this.doGetRequest(this.getServiceUrl(EkUrl.CARD_COLLECTION));
		final String json = r.body().string();
		UserClient.logger.debug(json);
		this.parseJsonResponse(json);
		return json;
	}

	public List<CardCollect> getCardCollection() throws IOException {
		final String json = this.getCardCollectionAsJson();
		final Collection collection = UserClient.GSON.fromJson(json, Collection.class);
		return collection.getData().getCardCollect();
	}

	private class JsonResponse {
		private boolean	validStatus;
		private String	message;
	}
}
