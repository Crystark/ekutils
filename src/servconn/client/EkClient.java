package servconn.client;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.squareup.okhttp.Response;

import servconn.dto.card.Card;
import servconn.dto.card.CardData;
import servconn.dto.league.LeagueData;
import servconn.dto.login.Player;
import servconn.dto.rune.Rune;
import servconn.dto.rune.RuneData;
import servconn.dto.skill.Skill;
import servconn.dto.skill.SkillData;

public class EkClient extends MrUserClient {
	private Map<String, Card>	cardMap;
	private Map<String, Skill>	skillMap;
	private Map<String, Rune>	runeMap;

	EkClient() {}

	protected String getConnectionData() throws IOException {
		final Response response = this.doGetRequest("http://mobile.arcgames.com//user/playasguest?gameid=51&platform=android&sdkvcode=2.2.2&androidos=17");
		final String json = response.body().string();
		final Player p = this.getPlayerFromConnectionData(json);
		this.loginData.setPlayer(p);
		return json;
	}

	public String getLeagueDataAsJson(final String server) throws IOException {
		this.login(server);
		final Response r = this.doGetRequest(this.loginData.getServerUrl() + "/league.php?do=getLeagueInfo&phpl=EN");
		final String json = r.body().string();
		return json;
	}

	public LeagueData getLeagueData(final String server) throws IOException {
		final String json = this.getLeagueDataAsJson(server);
		final LeagueData leagueData = EkClient.GSON.fromJson(json, LeagueData.class);
		return leagueData;
	}

	public String getServerCardsAsJson(final String server) throws IOException {
		this.login(server);
		final Response r = this.doGetRequest(this.loginData.getServerUrl() + "/card.php?do=GetAllCard&phpl=EN");
		final String json = r.body().string();
		return json;
	}

	public Map<String, Card> getServerCards(final String server) throws IOException {
		final String json = this.getServerCardsAsJson(server);
		final CardData cardData = EkClient.GSON.fromJson(json, CardData.class);
		final List<Card> cardList = cardData.getData().getCards();
		this.cardMap = new HashMap<String, Card>();
		for (final Card card : cardList) {
			this.cardMap.put(card.getCardId(), card);
		}
		return this.cardMap;
	}

	public String getServerSkillsAsJson(final String server) throws IOException {
		this.login(server);
		final Response r = this.doGetRequest(this.loginData.getServerUrl() + "/card.php?do=GetAllSkill&phpl=EN");
		final String json = r.body().string();
		return json;
	}

	public Map<String, Skill> getServerSkills(final String server) throws IOException {
		final String json = this.getServerSkillsAsJson(server);
		final SkillData skillData = EkClient.GSON.fromJson(json, SkillData.class);
		final List<Skill> skillList = skillData.getData().getSkills();
		this.skillMap = new HashMap<String, Skill>();
		for (final Skill skill : skillList) {
			this.skillMap.put(skill.getSkillId().toString(), skill);
		}
		return this.skillMap;
	}

	public String getServerRunesAsJson(final String server) throws IOException {
		this.login(server);
		final Response r = this.doGetRequest(this.loginData.getServerUrl() + "/rune.php?do=GetAllRune&phpl=EN");
		final String json = r.body().string();
		return json;
	}

	public Map<String, Rune> getServerRunes(final String server) throws IOException {
		final String json = this.getServerRunesAsJson(server);
		this.runeMap = new HashMap<String, Rune>();
		final RuneData runeData = EkClient.GSON.fromJson(json, RuneData.class);
		final List<Rune> runeList = runeData.getData().getRunes();
		for (final Rune rune : runeList) {
			this.runeMap.put(rune.getRuneId(), rune);
		}
		return this.runeMap;
	}
}
