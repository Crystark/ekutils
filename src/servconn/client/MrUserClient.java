package servconn.client;

import java.io.IOException;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import servconn.client.MrUserClient.LoginResponse.ReturnObjs;
import servconn.dto.user.UserInfo;

public class MrUserClient extends UserClient {
	MrUserClient() {}

	MrUserClient(final String email, final String password) {
		this.loginData.setEmail(email);
		this.loginData.setPassword(password);
	}

	@Override
	protected void login(final String server) throws IOException {
		login();
	}

	@Override
	protected synchronized void login() throws IOException {
		if (!this.loginData.isLoggedIn()) {
			LoginData ld;
			if (this.loginData.getEmail() != null) {
				ld = new LoginData().login(this.loginData.getEmail(), this.loginData.getPassword());
			}
			else {
				ld = new LoginData().quickStart();
			}
			login(ld);
			this.loginData.setLoggedIn(true);
			if (this.loginData.getUserInfo() == null) {
				final UserInfo userInfo = this.getUserInfo();
				this.loginData.setUserInfo(userInfo);
			}
		}
	}

	@Override
	protected String getServiceUrl(final String url) {
		final StringBuilder sb = new StringBuilder();
		sb.append(this.loginData.getServerUrl());
		sb.append(url);
		sb.append("&phpl=EN");
		return sb.toString();
	}

	void login(LoginData ld) throws IOException {
		ReturnObjs lr = postLoginData(ld);

		this.loginData.setServer(lr.GS_NAME);
		this.loginData.setServerUrl(lr.GS_IP);

		RequestBody form = new FormEncodingBuilder()
			.add("Udid", ld.callPara.udid)
			.add("time", "" + lr.timestamp)
			.add("UserName", lr.userName)
			.add("Origin", "ANDROID")
			.add("key", lr.key)
			.add("Password", lr.U_ID)
			.add("Devicetoken", "")
			.build();

		doPostRequest(this.loginData.getServerUrl() + "/login.php?do=PassportLogin", form);
	}

	ReturnObjs postLoginData(LoginData ld) throws IOException {
		Response r = doPostRequest(
			"http://im.fantasytoyou.com/pp/httpService.do",
			RequestBody.create(
				MediaType.parse("application/json; charset=utf-8"),
				GSON.toJson(ld)));
		LoginResponse lr = GSON.fromJson(r.body().string(), LoginResponse.class);
		if (lr.returnCode != 0) {
			throw new IllegalArgumentException("Failed to login: " + lr.returnMsg);
		}
		return lr.returnObjs;
	}

	//	protected String getConnectionData() throws IOException {
	//		final Response response = this.doGetRequest("http://mobile.arcgames.com/user/login?gameid=51");
	//		final String body = response.body().string();
	//		MrUserClient.logger.debug("getConnectionData login");
	//		MrUserClient.logger.debug(body);
	//		final Document doc = Jsoup.parse(body);
	//		final Element userEl = doc.getElementById("un");
	//		final String nameOfEmailField = userEl.attr("name");
	//		final Element passEl = doc.getElementById("pw");
	//		final String nameOfPasswordField = passEl.attr("name");
	//		final Element captchaEl = doc.getElementById("captcha_login");
	//		final String nameOfCaptchaField = captchaEl.attr("name");
	//		MrUserClient.logger.debug("nameOfEmailField: " + nameOfEmailField);
	//		MrUserClient.logger.debug("nameOfPasswordField: " + nameOfPasswordField);
	//		MrUserClient.logger.debug("nameOfCaptchaField: " + nameOfCaptchaField);
	//		final RequestBody formBody = new FormEncodingBuilder().add(nameOfEmailField, this.loginData.getEmail()).add(nameOfPasswordField, this.loginData.getPassword()).add(nameOfCaptchaField, "").build();
	//		final Response r = this.doPostRequest("http://mobile.arcgames.com/user/login?gameid=51", formBody);
	//		final String json = r.body().string();
	//		MrUserClient.logger.debug("doLogin, submit form with email / password");
	//		MrUserClient.logger.debug(json);
	//		final Player p = this.getPlayerFromConnectionData(json);
	//		this.loginData.setPlayer(p);
	//		MrUserClient.logger.debug(p);
	//		return json;
	//	}

	public class LoginData {
		public CallPara	callPara	= new CallPara();
		public String	serviceName;

		public LoginData login(String user, String pass) {
			this.serviceName = "login";
			this.callPara.userName = user;
			this.callPara.userPassword = pass;

			return this;
		}

		public LoginData quickStart() {
			this.serviceName = "startGameJson";

			return this;
		}

		public class CallPara {
			public String	gameName		= "SGZJ-ANDROID-SG";
			public String	clientType		= "EN";
			public String	releaseChannel	= "EN";
			public String	locale			= "EN";
			public String	udid			= "08c5831b8ccc531b79a78f98cde6e27dbcb5cb9a";

			public String	userPassword;
			public String	userName;
		}

	}

	public class LoginResponse {
		public int			returnCode;
		public String		returnMsg;
		public ReturnObjs	returnObjs;

		public class ReturnObjs {
			public int		GS_CHAT_PORT;
			public String	GS_ID;
			public String	GS_CHAT_IP;
			public String	GS_NAME;
			public String	initialUName;
			public int		GS_PORT;
			public String	friendCode;
			public String	source;
			public String	userName;
			public String	U_ID;
			public String	uEmailState;
			public String	LOGIN_TYPE;
			public String	GS_DESC;
			public String	GS_IP;
			public String	G_TYPE;
			public String	key;
			public long		timestamp;
		}
	}
}
