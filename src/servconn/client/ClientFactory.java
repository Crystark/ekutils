package servconn.client;

public class ClientFactory {
	public static EkClient get() {
		return new EkClient();
	}

	public static UserClient get(String server, String login, String password) {
		return get(Server.valueOf(server), login, password);
	}

	public static UserClient get(Server server, String login, String password) {
		switch (server.game) {
			case ek:
				throw new RuntimeException("EK is dead");
			case mr:
				return new MrUserClient(login, password);
			default:
				throw new RuntimeException("Unknown game " + server.game);
		}
	}
}
