// 
// EkUtils 
// 

package ekdao.dao;

import java.util.List;
import servconn.dto.card.Card;

public interface CardDao
{
    Card getCard(final String p0);
    
    Card getCardById(final String p0);
    
    Card getCardByName(final String p0);
    
    Integer getCardHpByLevel(final String p0, final Integer p1);
    
    Integer getCardAttackByLevel(final String p0, final Integer p1);
    
    List<String> getFactionCardsByStar(final String p0);
    
    List<String> getSpecialCards();
    
    List<String> getGoldCards();
    
    List<String> getExperienceCards();
    
    List<String> getDemons();
    
    List<String> getHydras();
    
    List<String> getEWBosses();
}
