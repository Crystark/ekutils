// 
// EkUtils 
// 

package ekdao.dao;

import java.util.List;
import servconn.dto.card.Card;
import servconn.dto.skill.Skill;

public interface SkillDao
{
    Skill getSkill(final String p0);
    
    Skill getSkillById(final String p0);
    
    Skill getSkillByName(final String p0);
    
    List<String> getCardSkills(final Card p0);
}
