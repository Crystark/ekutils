package ekdao.dao;

import org.apache.logging.log4j.LogManager;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import servconn.dto.card.Card;
import org.apache.commons.lang3.StringUtils;
import java.util.List;
import java.io.FileNotFoundException;
import java.io.FileReader;
import servconn.dto.skill.SkillData;
import java.io.File;
import java.util.HashMap;
import org.apache.logging.log4j.Logger;
import servconn.dto.skill.Skill;
import java.util.Map;
import com.google.gson.Gson;

public class SkillDaoImpl implements SkillDao
{
    private static Gson GSON;
    private Map<String, Skill> skillIdMap;
    private Map<String, Skill> skillNameMap;
    private static final Logger logger;
    private Skill defaultSkill;
    
    SkillDaoImpl(final File file) {
        this.skillIdMap = new HashMap<String, Skill>();
        this.skillNameMap = new HashMap<String, Skill>();
        try {
            final SkillData skillData = SkillDaoImpl.GSON.fromJson(new FileReader(file), SkillData.class);
            final List<Skill> skillList = skillData.getData().getSkills();
            for (final Skill skill : skillList) {
                this.skillIdMap.putIfAbsent(skill.getSkillId().toString(), skill);
                this.skillNameMap.putIfAbsent(skill.getName().toLowerCase(), skill);
            }
            (this.defaultSkill = new Skill()).setName("");
        }
        catch (FileNotFoundException e) {
            SkillDaoImpl.logger.fatal(e);
        }
    }
    
    @Override
    public Skill getSkillById(final String skilId) {
        return this.skillIdMap.get(skilId);
    }
    
    @Override
    public Skill getSkillByName(final String skillName) {
        return this.skillNameMap.get(skillName.toLowerCase());
    }
    
    @Override
    public Skill getSkill(final String skillKey) {
        if (StringUtils.isEmpty(skillKey)) {
            return this.defaultSkill;
        }
        Skill skill = this.getSkillById(skillKey);
        if (skill == null) {
            skill = this.getSkillByName(skillKey);
        }
        return skill;
    }
    
    @Override
    public List<String> getCardSkills(final Card card) {
        final List<String> skills = new ArrayList<String>();
        Skill skill = this.getSkill(card.getSkill());
        if (skill != null) {
            skills.add(skill.getName());
        }
        else {
            SkillDaoImpl.logger.warn("Skill {} not found for card {}, update your skills.json file", card.getSkill(), card.getCardName());
        }
        skill = this.getSkill(card.getLockSkill1());
        if (skill != null) {
            skills.add(skill.getName());
        }
        else {
            SkillDaoImpl.logger.warn("Skill {} not found for card {}, update your skills.json file", card.getLockSkill1(), card.getCardName());
        }
        skill = this.getSkill(card.getLockSkill2());
        if (skill != null) {
            skills.add(skill.getName());
        }
        else {
            SkillDaoImpl.logger.warn("Skill {} not found for card {}, update your skills.json file", card.getLockSkill2(), card.getCardName());
        }
        return skills;
    }
    
    static {
        SkillDaoImpl.GSON = new GsonBuilder().setPrettyPrinting().create();
        logger = LogManager.getLogger();
    }
}
