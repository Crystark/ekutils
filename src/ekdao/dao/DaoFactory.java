// 
// EkUtils 
// 

package ekdao.dao;

import org.apache.logging.log4j.LogManager;
import java.io.File;
import org.apache.logging.log4j.Logger;

public final class DaoFactory
{
    private static final Logger logger;
    
    public static CardDao getCardDao(final String filePath) {
        final File file = checkFile(filePath);
        final CardDao cardDao = new CardDaoImpl(file);
        return cardDao;
    }
    
    public static SkillDao getSkillDao(final String filePath) {
        final File file = checkFile(filePath);
        final SkillDao skillDao = new SkillDaoImpl(file);
        return skillDao;
    }
    
    public static RuneDao getRuneDao(final String filePath) {
        final File file = checkFile(filePath);
        final RuneDao runeDao = new RuneDaoImpl(file);
        return runeDao;
    }
    
    private static File checkFile(final String filePath) {
        final File file = new File(filePath);
        DaoFactory.logger.debug(file.getAbsolutePath());
        if (!(file.exists() & file.isFile())) {
            DaoFactory.logger.fatal("File not found " + filePath);
            throw new IllegalArgumentException("File not found: " + System.getProperty("user.dir") + File.separator + filePath);
        }
        return file;
    }
    
    static {
        logger = LogManager.getLogger();
    }
}
