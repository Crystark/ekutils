// 
// EkUtils 
// 

package ekdao.util;

public enum CardFaction
{
    tundra("1"), 
    forest("2"), 
    swamp("3"), 
    mtn("4");
    
    private String val;
    
    private CardFaction(final String val) {
        this.val = val;
    }
    
    public String getValue() {
        return this.val;
    }
    
    public static String getEnumByString(final String code) {
        for (final CardFaction e : values()) {
            if (code.equals(e.val)) {
                return e.name();
            }
        }
        return "";
    }
}
