// 
// EkUtils 
// 

package ekdao.util;

public enum Buying
{
    boostercard("7"), 
    boosterpack("4"), 
    goldpack("1"), 
    goldpack10x("8"), 
    firetoken("3"), 
    firetoken10x("9");
    
    private String val;
    
    private Buying(final String val) {
        this.val = val;
    }
    
    public String getValue() {
        return this.val;
    }
}
