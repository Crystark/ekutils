// 
// EkUtils 
// 

package ekutils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;

import servconn.client.ClientFactory;
import servconn.client.EkClient;
import servconn.dto.league.Card;

public class MapApp
{
    private static String cardList;
    
    public static void main(final String[] args) throws IOException {
        final String[] cards = MapApp.cardList.split(",");
        final List<Card> cardListe = new ArrayList<Card>(cards.length);
        for (int i = 0; i < cards.length; ++i) {
            final String[] cardProps = cards[i].split("_");
            final Card c = new Card();
            c.setCardId(cardProps[0]);
            c.setLevel(cardProps[1]);
            if (cardProps.length == 3) {
                c.setSkillNew(cardProps[2]);
            }
            cardListe.add(c);
        }
        for (final Card c2 : cardListe) {
            System.out.println(c2);
        }
        final EkClient client = ClientFactory.get();
        final String jsonStr = client.getServerCardsAsJson("aeon");
        System.out.println(jsonStr);
        System.out.println(new GsonBuilder().setPrettyPrinting().create().toJson(new JsonParser().parse(jsonStr)));
    }
    
    static {
        MapApp.cardList = "7106_10,7082_10,181_15_688,320_15_688,323_15_688,310_15_688,257_15_1034,312_15_688,250_15_688,260_15_688";
    }
}
