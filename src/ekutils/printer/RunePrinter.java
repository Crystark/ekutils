// 
// EkUtils 
// 

package ekutils.printer;

import ekdao.dao.RuneDao;
import servconn.dto.userdeck.UserRuneInfo;

public interface RunePrinter
{
    String printRune(final UserRuneInfo p0);
    
    void setRuneDao(final RuneDao p0);
}
