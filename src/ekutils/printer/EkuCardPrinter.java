// 
// EkUtils 
// 

package ekutils.printer;

import servconn.dto.skill.Skill;
import servconn.dto.card.Card;
import org.apache.commons.lang3.StringUtils;
import servconn.dto.usercard.UserCard;
import ekdao.dao.SkillDao;
import ekdao.dao.CardDao;

public class EkuCardPrinter implements CardPrinter
{
    private CardDao cardDao;
    private SkillDao skillDao;
    
    @Override
    public String printCard(final UserCard userCard) {
        final Card card = this.cardDao.getCard(userCard.getCardId().toString());
        final StringBuilder sb = new StringBuilder();
        sb.append(card.getCardName()).append(":").append(userCard.getLevel());
        if (userCard.getSkillNew() != null && !StringUtils.isEmpty(userCard.getSkillNew().toString()) && !"0".equals(userCard.getSkillNew().toString())) {
            final Skill skill = this.skillDao.getSkill(userCard.getSkillNew().toString());
            if (skill != null) {
                sb.append(":").append(skill.getName().replace(":", ""));
            }
        }
        return sb.toString();
    }
    
    @Override
    public void setCardDao(final CardDao cardDao) {
        this.cardDao = cardDao;
    }
    
    @Override
    public void setSkillDao(final SkillDao skillDao) {
        this.skillDao = skillDao;
    }
}
