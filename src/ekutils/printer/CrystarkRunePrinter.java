// 
// EkUtils 
// 

package ekutils.printer;

import servconn.dto.rune.Rune;
import servconn.dto.userdeck.UserRuneInfo;
import ekdao.dao.RuneDao;

public class CrystarkRunePrinter implements RunePrinter
{
    private RuneDao runeDao;
    
    @Override
    public String printRune(final UserRuneInfo userRune) {
        final Rune rune = this.runeDao.getRune(userRune.getRuneId());
        final StringBuilder sb = new StringBuilder();
        sb.append(rune.getRuneName());
        sb.append(":L").append(userRune.getLevel());
        return sb.toString();
    }
    
    @Override
    public void setRuneDao(final RuneDao runeDao) {
        this.runeDao = runeDao;
    }
}
