// 
// EkUtils 
// 

package ekutils.printer;

public class RunePrinterFactory
{
    public static RunePrinter getRunePrinter(final String printerType) {
        if ("crys".equalsIgnoreCase(printerType)) {
            return new CrystarkRunePrinter();
        }
        if ("eku".equalsIgnoreCase(printerType)) {
            return new EkuRunePrinter();
        }
        return null;
    }
}
