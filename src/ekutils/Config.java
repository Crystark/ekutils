package ekutils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Vector;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Config
{
    static final Logger LOGGER;
    private static SortedProperties defaultProps;
    private static SortedProperties userProps;
    private static SortedProperties extraProps;
    private static String propertyFile;
    private static ResourceBundle messages;
    
    public static void initialize(final String propertyFile) {
        try {
            Config.propertyFile = propertyFile;
            final FileInputStream in = new FileInputStream(propertyFile);
            Config.defaultProps.load(in);
            in.close();
            validateProperties();
            createInternalProperties();
            final Locale locale = new Locale("en", "US");
            Config.messages = ResourceBundle.getBundle("ekutils.messages", locale);
        }
        catch (FileNotFoundException e) {
            throw new IllegalStateException(String.format("%s not found, aborting", propertyFile));
        }
        catch (IOException e2) {
            throw new IllegalStateException(String.format("Cannot read %s, check file system permissions, aborting", propertyFile));
        }
    }
    
    private static void createInternalProperties() {
        Config.userProps.put("email", Config.defaultProps.getProperty("email"));
        Config.userProps.put("password", Config.defaultProps.getProperty("password"));
        Config.userProps.put("defaultServer", Config.defaultProps.getProperty("defaultServer"));
        Config.userProps.put("hydraPoints", (Config.defaultProps.getProperty("hydraPoints") == null) ? "" : Config.defaultProps.getProperty("hydraPoints"));
        Config.extraProps.putAll(Config.defaultProps);
        Config.extraProps.remove("email");
        Config.extraProps.remove("password");
        Config.extraProps.remove("defaultServer");
        Config.extraProps.remove("hydraPoints");
    }
    
    public static Properties getProperties() {
        return Config.defaultProps;
    }
    
    public static String getProperty(final String key) {
        return Config.defaultProps.getProperty(key);
    }
    
    public static String getMessage(final String key) {
        return Config.messages.getString(key);
    }
    
    public static String findKeyByValue(final String value) {
        String key = "";
        for (final Map.Entry entry : Config.defaultProps.entrySet()) {
            if (value.equalsIgnoreCase((String)entry.getValue())) {
                key = (String)entry.getKey();
                break;
            }
        }
        return key;
    }
    
    private static void validateProperties() throws IllegalArgumentException {
        if (Config.defaultProps.isEmpty()) {
            throw new IllegalArgumentException(String.format("No property found in %s file", Config.propertyFile));
        }
        if (StringUtils.isEmpty(getProperty("email"))) {
            throw new IllegalArgumentException(String.format("email property in %s file does not exist or empty", Config.propertyFile));
        }
        if (StringUtils.isEmpty(getProperty("password"))) {
            throw new IllegalArgumentException(String.format("password property in {} file does not exist or empty", Config.propertyFile));
        }
        if (StringUtils.isEmpty(getProperty("defaultServer"))) {
            throw new IllegalArgumentException(String.format("defaultServer property in {} file does not exist or empty", Config.propertyFile));
        }
        if (StringUtils.isEmpty(getProperty("hydraPoints"))) {
            Config.LOGGER.warn("Define hydrapoints in your {} file", Config.propertyFile);
            Config.LOGGER.info("hydraPoints=200, 600, 1800, 6000, 23000" + UtilCons.NL);
        }
        else {
            final String[] dpArray = getProperty("hydraPoints").split(",");
            if (dpArray.length != 5) {
                throw new IllegalArgumentException("wrong format for hydraPoints. The correct format is hydraPoints=200, 600, 1800, 6000, 23000");
            }
        }
        final String server = getProperty("defaultServer");
        if (!UtilCons.SERVERS.contains(server)) {
            throw new IllegalArgumentException("defaultServer property is invalid. Valid servers are: " + Arrays.toString(UtilCons.SERVER_VALUES));
        }
    }
    
    public static boolean fileExists(final String filename) {
        final File f = new File(filename);
        return f.exists() && !f.isDirectory();
    }
    
    public static boolean writePropertiesToFile() throws IOException {
        final File f = new File(Config.propertyFile);
        final FileWriter fw = new FileWriter(f);
        Config.userProps.store(fw, "User credentials");
        Config.extraProps.store(fw, getMessage("extraPropertyHelp"));
        return true;
    }
    
    public static String addProperty(final String property, final String value) {
        final String oldPropValue = (String)Config.extraProps.setProperty(property, value);
        Config.defaultProps.setProperty(property, value);
        return oldPropValue;
    }
    
    static {
        LOGGER = LogManager.getLogger();
        Config.defaultProps = new SortedProperties();
        Config.userProps = new SortedProperties();
        Config.extraProps = new SortedProperties();
    }
    
    private static class SortedProperties extends Properties
    {
        @Override
        public Enumeration keys() {
            final Enumeration keysEnum = super.keys();
            final Vector<String> keyList = new Vector<String>();
            while (keysEnum.hasMoreElements()) {
                keyList.add((String)keysEnum.nextElement());
            }
            Collections.sort(keyList);
            return keyList.elements();
        }
    }
}
