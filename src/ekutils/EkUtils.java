package ekutils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multisets;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import ekdao.dao.CardDao;
import ekdao.dao.DaoFactory;
import ekdao.dao.SkillDao;
import ekdao.util.CardFaction;
import servconn.client.ClientFactory;
import servconn.client.EkClient;
import servconn.dto.card.Card;
import servconn.dto.card.CardData;
import servconn.dto.maze.MazeAwardCardChip;
import servconn.dto.skill.Skill;

public class EkUtils {
	protected static Gson	GSON;
	static final Logger		LOGGER;

	public static String prettyPrintJson(final String uglyJson) {
		final Gson gson = new GsonBuilder().setPrettyPrinting().create();
		final JsonParser jp = new JsonParser();
		final JsonElement je = jp.parse(uglyJson);
		final String prettyJsonString = gson.toJson(je);
		return prettyJsonString;
	}

	public static void saveServerCardsToFile(final String server, final String filePath) throws IOException {
		final EkClient client = ClientFactory.get();
		EkUtils.LOGGER.info("Downloading cards...");
		final String json = client.getServerCardsAsJson(server);
		final String formattedJson = prettyPrintJson(json);
		writeToFile(filePath, formattedJson);
	}

	public static void saveServerRunesToFile(final String server, final String filePath) throws IOException {
		final EkClient client = ClientFactory.get();
		EkUtils.LOGGER.info("Downloading runes...");
		final String json = client.getServerRunesAsJson(server);
		final String formattedJson = prettyPrintJson(json);
		writeToFile(filePath, formattedJson);
	}

	public static void saveServerSkillsToFile(final String server, final String filePath) throws IOException {
		final EkClient client = ClientFactory.get();
		EkUtils.LOGGER.info("Downloading skills...");
		final String json = client.getServerSkillsAsJson(server);
		final String formattedJson = prettyPrintJson(json);
		writeToFile(filePath, formattedJson);
	}

	public static void writeToFile(final String filePath, final String json) {
		try {
			final FileWriter fw = new FileWriter(filePath, false);
			final BufferedWriter bw = new BufferedWriter(fw);
			bw.write(json);
			bw.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void displayKWPool() throws FileNotFoundException {
		final BufferedReader br = new BufferedReader(new FileReader(UtilCons.CARDSFILE));
		final CardData cardData = EkUtils.GSON.fromJson(br, CardData.class);
		final List<Card> cardList = cardData.getData().getCards();
		final String format = "%1$-20s|%2$-6s\n";
		System.out.println("KW Pool");
		for (final Card card : cardList) {
			if (!card.getForceFightExchange().equals("0")) {
				System.out.format(format, card.getCardName(), card.getForceExchangeInitPrice());
			}
		}
	}

	public static void displayThiefDrops() throws FileNotFoundException {
		final BufferedReader br = new BufferedReader(new FileReader(UtilCons.CARDSFILE));
		final CardData cardData = EkUtils.GSON.fromJson(br, CardData.class);
		final List<Card> cardList = cardData.getData().getCards();
		final String format = "%1$-20s\n";
		System.out.println("Thief Drops");
		for (final Card card : cardList) {
			if (!card.getRobber().equals("0")) {
				System.out.format(format, card.getCardName());
			}
		}
	}

	public static void printAllCards() throws FileNotFoundException {
		final StringBuilder sb = new StringBuilder();
		final StringBuilder sb2 = new StringBuilder();
		final CardDao cardDao = DaoFactory.getCardDao(UtilCons.CARDSFILE);
		final SkillDao skillDao = DaoFactory.getSkillDao(UtilCons.SKILLSFILE);
		final BufferedReader br = new BufferedReader(new FileReader(UtilCons.CARDSFILE));
		final CardData cardData = EkUtils.GSON.fromJson(br, CardData.class);
		final List<Card> cardList = cardData.getData().getCards();
		for (final Card card : cardList) {
			sb.append(card.getCardName()).append(", ").append(card.getCost()).append(", ").append(card.getWait()).append(", ");
			sb.append(cardDao.getCardAttackByLevel(card.getCardId(), 10)).append(", ").append(cardDao.getCardHpByLevel(card.getCardId(), 10)).append(", ");
			sb.append(CardFaction.getEnumByString(card.getRace())).append(", ");
			final Skill skill1 = skillDao.getSkill(card.getSkill());
			final Skill skill2 = skillDao.getSkill(card.getLockSkill1());
			final Skill skill3 = skillDao.getSkill(card.getLockSkill2());
			if (skill1 != null) {
				sb.append(skill1.getName()).append(", ");
			}
			if (skill2 != null) {
				sb.append(skill2.getName()).append(", ");
			}
			if (skill3 != null) {
				sb.append(skill3.getName()).append(", ");
			}
			sb2.append(sb.substring(0, sb.length() - 2));
			sb2.append(UtilCons.NL);
			final String[] evoCardArray = card.getKeyCard().split("_");
			final Card evoCard = cardDao.getCard(evoCardArray[0]);
			sb2.append(evoCard.getCardName() + " x " + evoCardArray[1]).append(UtilCons.NL).append(UtilCons.NL);
			sb.setLength(0);
		}
		System.out.println(sb2.toString());
	}

	public static String printCardDrops(final List<String> cardIdList, final List<MazeAwardCardChip> cardFragmentList) {
		final StringBuilder sb = new StringBuilder();
		final CardDao cardDao = DaoFactory.getCardDao(UtilCons.CARDSFILE);
		if (cardIdList != null && !cardIdList.isEmpty()) {
			sb.append(UtilCons.NL).append("***You got these cards***").append(UtilCons.NL);
			cardIdList.stream()
				.map(cardDao::getCard)
				.sorted(new Comparator<Card>() {
					@Override
					public int compare(Card o1, Card o2) {
						int compareGroup = getDropGroup(o1).compareTo(getDropGroup(o2));
						if (compareGroup == 0) {
							int compareStars = o1.getRank().compareTo(o2.getRank());
							if (compareStars == 0) {
								return o1.getCardName().compareTo(o2.getCardName());
							}
							return compareStars;
						}
						return compareGroup;
					}

					Integer getDropGroup(Card card) {
						if (card.isGold()) {
							return 1;
						}
						else if (card.isFeast()) {
							return 2;
						}
						else if (card.isSpecial()) {
							return 3;
						}
						else if ("4".equals(card.getColor())) {
							return 4;
						}
						else if ("5".equals(card.getColor())) {
							return 5;
						}
						return 0;
					}
				})
				.map(card -> {
					String prefix = "";
					if (card.isSpecial()) {
						prefix = "[SPECIAL] ";
					}
					else if (card.isGold()) {
						prefix = "[GOLD   ] ";
					}
					else if (card.isFeast()) {
						prefix = "[FEAST  ] ";
					}
					else if ("4".equals(card.getColor())) {
						prefix = "[4 STAR ] ";
					}
					else if ("5".equals(card.getColor())) {
						prefix = "[5 STAR ] ";
					}
					return prefix + card.getCardName();
				})
				.collect(Collectors.groupingBy(s -> s, LinkedHashMap::new, Collectors.counting()))
				.forEach((s, c) -> {
					sb.append(String.format("%3d x %s", c, s)).append(UtilCons.NL);
				});
		}
		if (cardFragmentList != null && !cardFragmentList.isEmpty()) {
			final Multiset<String> cardFragmentCount = HashMultiset.create();
			for (final MazeAwardCardChip cardFragment : cardFragmentList) {
				cardFragmentCount.add(cardDao.getCard(cardFragment.getId()).getCardName(), Integer.parseInt(cardFragment.getNum()));
			}
			sb.append(UtilCons.NL).append("***You got these card fragments***").append(UtilCons.NL);
			for (final String cardName2 : Multisets.copyHighestCountFirst(cardFragmentCount).elementSet()) {
				sb.append(String.format("%3d x %s", cardFragmentCount.count(cardName2), cardName2)).append(UtilCons.NL);
			}
		}
		return sb.toString();
	}

	static {
		EkUtils.GSON = new GsonBuilder().setPrettyPrinting().create();
		LOGGER = LogManager.getLogger();
	}
}
