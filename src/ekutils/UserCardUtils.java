// 
// EkUtils 
// 

package ekutils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ekdao.dao.CardDao;
import ekdao.dao.DaoFactory;
import ekdao.dao.SkillDao;
import ekutils.printer.CrystarkCardPrinter;
import servconn.client.UserClient;
import servconn.client.UserClient;
import servconn.dto.card.Card;
import servconn.dto.card.CardCollect;
import servconn.dto.card.CardFragment;
import servconn.dto.usercard.UserCard;

public class UserCardUtils
{
    static final Logger LOGGER;
    private UserClient client;
    private CardDao cardDao;
    private SkillDao skillDao;
    private CrystarkCardPrinter cardPrinter;
    private boolean fourStars;
    private boolean fiveStars;
    private boolean specialCards;
    private boolean feastCards;
    private boolean goldCards;
    private String previousStar;
    
    private UserCardUtils() {
        this.previousStar = "";
    }
    
    public UserCardUtils(UserClient client) {
        this.previousStar = "";
        this.client = client;
        this.cardDao = DaoFactory.getCardDao(UtilCons.CARDSFILE);
        this.skillDao = DaoFactory.getSkillDao(UtilCons.SKILLSFILE);
        (this.cardPrinter = new CrystarkCardPrinter()).setCardDao(this.cardDao);
        this.cardPrinter.setSkillDao(this.skillDao);
    }
    
    public void printUserCards() throws IOException {
        final List<UserCard> userCardList = this.client.getUserCards();
        final StringBuilder sb = new StringBuilder();
        if (this.fiveStars) {
            final List<String> cardIdList5Star = this.cardDao.getFactionCardsByStar("5");
            sb.append(this.filterUserCardsByList(userCardList, cardIdList5Star, "5 star cards"));
        }
        if (this.fourStars) {
            final List<String> cardIdList4Star = this.cardDao.getFactionCardsByStar("4");
            sb.append(this.filterUserCardsByList(userCardList, cardIdList4Star, "4 star cards"));
        }
        if (this.specialCards) {
            final List<String> cardIdListSpecial = this.cardDao.getSpecialCards();
            sb.append(this.filterUserCardsByList(userCardList, cardIdListSpecial, "Special cards"));
        }
        if (this.goldCards) {
            final List<String> cardIdListGold = this.cardDao.getGoldCards();
            sb.append(this.filterUserCardsByList(userCardList, cardIdListGold, "Gold cards"));
        }
        if (this.feastCards) {
            final List<String> cardIdListFeast = this.cardDao.getExperienceCards();
            sb.append(this.filterUserCardsByList(userCardList, cardIdListFeast, "Feast cards"));
        }
        System.out.println(sb.toString());
    }
    
    private String filterUserCardsByList(final List<UserCard> userCardList, final List<String> filterList, final String header) {
        final List<String> cardList = new ArrayList<String>();
        final SortedSet<String> cardSet = new TreeSet<String>();
        final StringBuilder sb = new StringBuilder();
        sb.append("**" + header + "**").append(UtilCons.NL).append(UtilCons.NL);
        if (userCardList == null || userCardList.isEmpty()) {
            sb.append("No card found...").append(UtilCons.NL);
        }
        else {
            for (final UserCard userCard : userCardList) {
                if (filterList.contains(userCard.getCardId().toString())) {
                    final String card = this.cardPrinter.printCard(userCard);
                    cardList.add(card);
                    cardSet.add(card);
                }
            }
            sb.append(this.printCardList(cardSet, cardList));
        }
        return sb.toString();
    }
    
    private String printCardList(final SortedSet<String> cardSet, final List<String> cardList) {
        final StringBuilder sb = new StringBuilder();
        for (final String card : cardSet) {
            sb.append(String.format("%4d x %s", Collections.frequency(cardList, card), card)).append(UtilCons.NL);
        }
        sb.append(UtilCons.NL);
        return sb.toString();
    }
    
    public void printCardFragments() throws IOException {
        final List<CardFragment> cardFragmentList = this.client.getCardFragments();
        if (cardFragmentList == null || cardFragmentList.isEmpty()) {
            return;
        }
        for (final CardFragment cardFragment : cardFragmentList) {
            final Card card = this.cardDao.getCard(cardFragment.getCardId().toString());
            cardFragment.setCardName(card.getCardName());
        }
        final Comparator<CardFragment> byStar = (e1, e2) -> e2.getStar().compareTo(e1.getStar());
        final Comparator<CardFragment> byName = (e1, e2) -> e1.getCardName().compareToIgnoreCase(e2.getCardName());
        cardFragmentList.stream().sorted(byStar.thenComparing(byName)).forEach(e -> {
            if (!this.previousStar.equals(e.getStar())) {
                System.out.println(UtilCons.NL + e.getStar() + " star fragments");
                this.previousStar = e.getStar();
            }
            this.printCardFragment(e);
        });
    }
    
    private void printCardFragment(final CardFragment fragment) {
        System.out.println(String.format("%-20s %4d / %3d (%d)", fragment.getCardName(), fragment.getChipNum(), fragment.getChipAmount(), fragment.getChipNum() / fragment.getChipAmount()));
    }
    
    public void setCardTypes(final List<String> cardTypes) {
        this.setFourStars(cardTypes.contains("4star"));
        this.setFiveStars(cardTypes.contains("5star"));
        this.setGoldCards(cardTypes.contains("gold"));
        this.setFeastCards(cardTypes.contains("feast"));
        this.setSpecialCards(cardTypes.contains("special"));
    }
    
    public void setFourStars(final boolean fourStars) {
        this.fourStars = fourStars;
    }
    
    public void setFiveStars(final boolean fiveStars) {
        this.fiveStars = fiveStars;
    }
    
    public void setSpecialCards(final boolean specialCards) {
        this.specialCards = specialCards;
    }
    
    public void setFeastCards(final boolean feastCards) {
        this.feastCards = feastCards;
    }
    
    public void setGoldCards(final boolean goldCards) {
        this.goldCards = goldCards;
    }
    
    public void printCollectionCards() throws IOException {
        System.out.println(UtilCons.NL + "---Collection Cards---");
        final List<CardCollect> cardCollectionList = this.client.getCardCollection();
        for (final CardCollect cardCollection : cardCollectionList) {
            final Card card = this.cardDao.getCard(cardCollection.getCardId() + "");
            System.out.println(card.getCardName() + ": " + cardCollection.getCollectNum());
        }
    }
    
    static {
        LOGGER = LogManager.getLogger();
    }
}
