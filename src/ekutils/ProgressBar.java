// 
// EkUtils 
// 

package ekutils;

public class ProgressBar
{
    private StringBuilder progress;
    
    public ProgressBar() {
        this.init();
    }
    
    public void update(int done, final int total) {
        final char[] workchars = { '|', '/', '-', '\\' };
        final String format = "\r%3d%% %s %c";
        final int percent = ++done * 100 / total;
        int extrachars = percent / 2 - this.progress.length();
        while (extrachars-- > 0) {
            this.progress.append('#');
        }
        System.out.printf(format, percent, this.progress, workchars[done % workchars.length]);
        if (done == total) {
            System.out.flush();
            System.out.println();
            this.init();
        }
    }
    
    private void init() {
        this.progress = new StringBuilder(60);
    }
}
