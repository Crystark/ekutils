package ekutils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ekdao.dto.UnifiedCard;
import servconn.client.UserClient;
import servconn.client.UserClient;
import servconn.dto.user.UserInfo;
import servconn.dto.usercard.UserCard;

public class SellingUtils
{
    private UserClient client;
    private Integer limit;
    static final Logger LOGGER;
    private List<String> sellCardTypes;
    private static int SELLATONCE;
    private static final String ONE = "1";
    private static final String TWO = "2";
    private static final String THREE = "3";
    private static final String GOLD = "g";
    private static String[] allTypes;
    private static final String SELLPREFIX = "_";
    
    public SellingUtils(UserClient client) {
        this.limit = Integer.MAX_VALUE;
        this.sellCardTypes = new ArrayList<String>();
        this.client = client;
    }
    
    public void setSellLimit(final Integer limit) {
        this.limit = limit;
    }
    
    public void setSellArgument(final String argument) {
        if ("all".equals(argument)) {
            this.sellCardTypes = Arrays.asList(SellingUtils.allTypes);
        }
        else if ("golds".equals(argument)) {
            this.sellCardTypes.add("g");
        }
        else {
            this.sellCardTypes.add(argument.substring(0, 1));
        }
    }
    
    public void sellCards() throws IOException, InterruptedException {
        UserInfo userInfo = this.client.getUserInfo();
        final Integer coinsBefore = userInfo.getCoins();
        List<String> lockedUserCardIds = userInfo.getLockedUserCardIds();
        List<UserCard> userCardList = this.client.getUserCards();
        final StringBuilder sb = new StringBuilder();
        for (final String card : lockedUserCardIds) {
            sb.append(card).append(SELLPREFIX);
        }
        SellingUtils.LOGGER.debug("locked cards " + sb.toString());
        UserCardDb ucd = new UserCardDb(userCardList, lockedUserCardIds);
        final List<UnifiedCard> sellCardList = new ArrayList<UnifiedCard>();
        for (final String cardType : this.sellCardTypes) {
            if ("g".equals(cardType)) {
                final List<UnifiedCard> tmpList = ucd.getGoldCardsForSelling();
                SellingUtils.LOGGER.info("Gold cards are added, total {}", tmpList.size());
                sellCardList.addAll(tmpList);
            }
            else {
                final List<UnifiedCard> tmpList = ucd.getFactionCardsForSelling(cardType);
                SellingUtils.LOGGER.info("{} star cards are added, total {}", cardType, tmpList.size());
                sellCardList.addAll(tmpList);
            }
        }
        List<UnifiedCard> cardsToBeSold;
        if (this.limit < sellCardList.size()) {
            SellingUtils.LOGGER.info("{} of total {} sellable cards will be sold ", this.limit, sellCardList.size());
            cardsToBeSold = sellCardList.subList(0, this.limit);
        }
        else {
            SellingUtils.LOGGER.info("We will be selling all {} available cards", sellCardList.size());
            cardsToBeSold = sellCardList;
        }
        final int totalCardsToBeSold = cardsToBeSold.size();
        this.sellCards(cardsToBeSold);
        userInfo = this.client.getUserInfo();
        final Integer coinsAfter = userInfo.getCoins();
        lockedUserCardIds = userInfo.getLockedUserCardIds();
        userCardList = this.client.getUserCards();
        ucd = new UserCardDb(userCardList, lockedUserCardIds);
        SellingUtils.LOGGER.info("You sold {} cards and got {} gold back!", totalCardsToBeSold, coinsAfter - coinsBefore);
        SellingUtils.LOGGER.info("Final gold {} {}", coinsAfter, UtilCons.NL);
        SellingUtils.LOGGER.info("Number of 1 star cards left: {}", ucd.getNumberOfFactionCardsByStar("1"));
        SellingUtils.LOGGER.info("Number of 2 star cards left: {}", ucd.getNumberOfFactionCardsByStar("2"));
        SellingUtils.LOGGER.info("Number of 3 star cards left: {}", ucd.getNumberOfFactionCardsByStar("3"));
    }
    
    private void sellCards(final List<UnifiedCard> cardsToBeSold) throws IOException, InterruptedException {
        SellingUtils.LOGGER.info("Started selling cards...");
        final ProgressBar pb = new ProgressBar();
        int index = 0;
        final int totalCards = cardsToBeSold.size();
        final StringBuilder sb = new StringBuilder();
        for (final UnifiedCard card : cardsToBeSold) {
            ++index;
            final String userCardId = card.getUserCard().getUserCardId();
            sb.append(userCardId).append("_");
            if (index % SellingUtils.SELLATONCE == 0) {
                final String cardsJoined = sb.substring(0, sb.length() - 1);
                this.client.doSellCardsRunes(cardsJoined, "");
                Thread.sleep(200L);
                sb.setLength(0);
                pb.update(index, totalCards);
            }
        }
        if (sb.length() > 0) {
            final String cardsJoined2 = sb.substring(0, sb.length() - 1);
            this.client.doSellCardsRunes(cardsJoined2, "");
            sb.setLength(0);
            pb.update(index - 1, totalCards);
        }
    }
    
    static {
        LOGGER = LogManager.getLogger();
        SellingUtils.SELLATONCE = 1200;
        SellingUtils.allTypes = new String[] { ONE, TWO, THREE, GOLD };
    }
}
