package ekutils;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import ekdao.dao.CardDao;
import ekdao.dao.DaoFactory;
import ekdao.dto.UnifiedCard;
import ekdao.util.DaoConstants;
import servconn.dto.card.Card;
import servconn.dto.usercard.UserCard;

public class UserCardDb
{
    private List<UserCard> userCardList;
    private CardDao cardDao;
    private List<UnifiedCard> cardList;
    private List<String> lockedUserCardIds;
    
    public UserCardDb(final List<UserCard> userCardList, final List<String> lockedUserCardIds) {
        this.cardList = new ArrayList<UnifiedCard>();
        this.userCardList = userCardList;
        this.lockedUserCardIds = lockedUserCardIds;
        this.initialize();
    }
    
    private void initialize() {
        this.cardDao = DaoFactory.getCardDao(UtilCons.CARDSFILE);
        for (final UserCard userCard : this.userCardList) {
            final UnifiedCard uc = new UnifiedCard();
            uc.setUserCard(userCard);
            final Card card = this.cardDao.getCard(userCard.getCardId().toString());
            uc.setCard(card);
            this.cardList.add(uc);
        }
    }
    
    public Predicate<UnifiedCard> isStarEquals(final String star) {
        return (Predicate<UnifiedCard>)(uc -> uc.getCard().getColor().equals(star));
    }
    
    public Predicate<UnifiedCard> belongsToFaction(final String faction) {
        return (Predicate<UnifiedCard>)(uc -> uc.getCard().getRace().equals(faction));
    }
    
    public Predicate<UnifiedCard> isFactionCard() {
        return (Predicate<UnifiedCard>)(uc -> DaoConstants.FACTIONS.contains(uc.getCard().getRace()));
    }
    
    public Predicate<UnifiedCard> isUnenchanted() {
        return (Predicate<UnifiedCard>)(uc -> uc.getUserCard().getLevel() == 0);
    }
    
    public Predicate<UnifiedCard> isLocked() {
        return (Predicate<UnifiedCard>)(uc -> this.lockedUserCardIds.contains(uc.getUserCard().getUserCardId().toString()));
    }
    
    public Predicate<UnifiedCard> isNotLocked() {
        return (Predicate<UnifiedCard>)(uc -> !this.lockedUserCardIds.contains(uc.getUserCard().getUserCardId().toString()));
    }
    
    public Predicate<UnifiedCard> isGold() {
        return (Predicate<UnifiedCard>)(uc -> "95".equals(uc.getCard().getRace()));
    }
    
    public Predicate<UnifiedCard> isFeast() {
        return (Predicate<UnifiedCard>)(uc -> "96".equals(uc.getCard().getRace()));
    }
    
    public Predicate<UnifiedCard> isSpecial() {
        return (Predicate<UnifiedCard>)(uc -> "99".equals(uc.getCard().getRace()));
    }
    
    public List<UnifiedCard> getFactionCardsForSelling(final String star) {
        return (List<UnifiedCard>)this.cardList.stream().filter(this.isFactionCard()).filter(this.isStarEquals(star)).filter(this.isNotLocked()).filter(this.isUnenchanted()).collect(Collectors.toList());
    }
    
    public List<UnifiedCard> getGoldCardsForSelling() {
        return (List<UnifiedCard>)this.cardList.stream().filter(this.isGold()).filter(this.isNotLocked()).filter(this.isUnenchanted()).collect(Collectors.toList());
    }
    
    public Integer getNumberOfFactionCardsByStar(final String star) {
        return (this.cardList.stream().filter(this.isFactionCard()).filter(this.isStarEquals(star)).collect(Collectors.toList())).size();
    }
}
