package ekutils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multisets;

import ekdao.dao.DaoFactory;
import ekdao.dao.RuneDao;
import servconn.client.UserClient;
import servconn.dto.meditate.MeditateDealData;
import servconn.dto.meditate.MeditateDealReward;
import servconn.dto.meditate.MeditateDealSell;
import servconn.dto.rune.Rune;

public class MeditateUtils {
	private UserClient							client;
	private RuneDao								runeDao;
	private static final int					SHARD			= 1;
	private static final int					SHARD_BLUE		= 3;
	private static final int					SHARD_PURPLE	= 4;
	private static final int					SHARD_GOLD		= 5;
	private static final Map<Integer, String>	shardMap;
	private static final int					RUNE			= 2;
	private static final int					BROKENSHARD		= 3;

	public MeditateUtils(UserClient client) {
		this.client = client;
		this.runeDao = DaoFactory.getRuneDao(UtilCons.RUNESFILE);
	}

	public void meditate(final Integer numOfMeditations) throws IOException {
		int numOfMultipleMeditations = numOfMeditations / 10;
		int numOfSingleMeditations = numOfMeditations % 10;
		final List<MeditateDealReward> totalRewards = new ArrayList<MeditateDealReward>();
		final List<MeditateDealSell> totalSells = new ArrayList<MeditateDealSell>();
		this.client.doMeditateDeal();
		final ProgressBar pb = new ProgressBar();
		for (int i = 0; i < numOfMultipleMeditations; ++i) {
			try {
				this.client.doMeditateMultipleAsJson();
			}
			catch (IllegalStateException e) {
				if (e.getMessage().equals("You've already used 'em all up.")) {
					numOfSingleMeditations += (numOfMultipleMeditations - i) * 10;
					numOfMultipleMeditations = i; // fix for progress bar
					break;
				}
				throw e;
			}
			final MeditateDealData meditateDealData = this.client.doMeditateDeal();
			final List<MeditateDealReward> rewards = meditateDealData.getRewards();
			final List<MeditateDealSell> sells = meditateDealData.getSells();
			totalRewards.addAll(rewards);
			totalSells.addAll(sells);
			pb.update(i, numOfMultipleMeditations + numOfSingleMeditations);
		}
		for (int i = 0; i < numOfSingleMeditations; ++i) {
			this.client.doMeditateOnce();
			if (i % 10 == 0 || i == numOfSingleMeditations - 1) {
				final MeditateDealData meditateDealData = this.client.doMeditateDeal();
				final List<MeditateDealReward> rewards = meditateDealData.getRewards();
				final List<MeditateDealSell> sells = meditateDealData.getSells();
				totalRewards.addAll(rewards);
				totalSells.addAll(sells);
			}
			pb.update(i + numOfMultipleMeditations, numOfMultipleMeditations + numOfSingleMeditations);
		}
		final int totalBrokenShards = this.getTotalBrokenShards(totalSells);
		System.out.println(String.format("Sold %d broken shards after meditations", totalBrokenShards));
		final String rewardList = this.getRewards(totalRewards);
		System.out.println(rewardList);
	}

	private String getRewards(final List<MeditateDealReward> totalRewards) {
		if (totalRewards == null || totalRewards.isEmpty()) {
			return "";
		}
		final StringBuilder sb = new StringBuilder();
		final Multiset<String> runeSet = HashMultiset.create();
		final Multiset<String> shardSet = HashMultiset.create();
		for (final MeditateDealReward reward : totalRewards) {
			if (reward.getType() == RUNE) {
				final Rune rune = this.runeDao.getRune(reward.getValue());
				runeSet.add(rune.getRuneName(), reward.getNum());
			}
			else {
				if (reward.getType() != SHARD) {
					continue;
				}
				shardSet.add(MeditateUtils.shardMap.get(Integer.parseInt(reward.getValue())), reward.getNum());
			}
		}
		if (!runeSet.isEmpty()) {
			sb.append(UtilCons.NL).append("***Runes from meditation***").append(UtilCons.NL);
			for (final String runeName : Multisets.copyHighestCountFirst(runeSet).elementSet()) {
				sb.append(String.format("%3d x %s", runeSet.count(runeName), runeName)).append(UtilCons.NL);
			}
		}
		if (!shardSet.isEmpty()) {
			sb.append(UtilCons.NL).append("***Shards from meditation***").append(UtilCons.NL);
			for (final String shardName : Multisets.copyHighestCountFirst(shardSet).elementSet()) {
				sb.append(String.format("%3d x %s", shardSet.count(shardName), shardName)).append(UtilCons.NL);
			}
		}
		return sb.toString();
	}

	private int getTotalBrokenShards(final List<MeditateDealSell> totalSells) {
		if (totalSells == null || totalSells.isEmpty()) {
			return 0;
		}
		int total = 0;
		for (final MeditateDealSell sell : totalSells) {
			total += sell.getNum();
		}
		return total;
	}

	static {
		final Map<Integer, String> aMap = new HashMap<Integer, String>();
		aMap.put(SHARD_BLUE, "Blue Shard");
		aMap.put(SHARD_PURPLE, "Purple Shard");
		aMap.put(SHARD_GOLD, "Gold Shard");
		shardMap = Collections.unmodifiableMap((Map<? extends Integer, ? extends String>) aMap);
	}
}
