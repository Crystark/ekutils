package ekutils;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ekutils.dto.MazePlayMode;
import ekutils.dto.MazeStats;
import servconn.client.UserClient;
import servconn.dto.maze.MazeAward;
import servconn.dto.maze.MazeAwardCardChip;
import servconn.dto.maze.MazeFloorInfoData;
import servconn.dto.maze.MazeInfoData;
import servconn.dto.user.UserInfo;
import servconn.dto.userdeck.UserDeckData;

public class MazeUtils {
	static final Logger				LOGGER;
	private static final Integer	CAN_CLEAR_MAZE;
	private static final Integer	MAZE_ALREADY_CLEARED;
	private static final Integer	CAN_RESET_MAZE;
	private static final Integer	MAZE_ALREADY_RESET;
	private static final Integer	CHEST;
	private static final Integer	MONSTER;
	private static final Integer	FLOOR_BOSS;
	private static final Integer	DEFAULT_STARTING_FLOOR;
	private UserClient				client;
	private static double			ENERGYPERBUY;

	public MazeUtils(UserClient client) {
		this.client = client;
	}

	private MazeInfoData getMazeInfo(final Integer mapStageId) throws IOException {
		final MazeInfoData mazeInfoData = this.client.showMazeInfoData(mapStageId);

		if (mapStageId == 99 && mazeInfoData != null) {
			mazeInfoData.setName("Kingdoms Tower");
		}

		MazeUtils.LOGGER.debug(mazeInfoData);
		return mazeInfoData;
	}

	public void playMaze(final int mapStageId, final MazePlayMode playMode) throws IOException, InterruptedException {
		MazeInfoData mazeInfoData = this.getMazeInfo(mapStageId);

		if (mazeInfoData.getClear().equals(MazeUtils.MAZE_ALREADY_CLEARED) && mazeInfoData.getFreeReset().equals(MazeUtils.MAZE_ALREADY_RESET)) {
			MazeUtils.LOGGER.warn("Maze {} already cleared and has no free reset currently, choose another maze", mazeInfoData.getName());
			return;
		}
		if (mazeInfoData.getClear().equals(MazeUtils.MAZE_ALREADY_CLEARED) && mazeInfoData.getFreeReset().equals(MazeUtils.CAN_RESET_MAZE)) {
			MazeUtils.LOGGER.info("Maze {} already cleared, doing free reset", mazeInfoData.getName());
			this.client.resetMaze(mapStageId);
			mazeInfoData = this.getMazeInfo(mapStageId);
		}
		if (mazeInfoData.getClear().equals(MazeUtils.CAN_CLEAR_MAZE)) {
			MazeUtils.LOGGER.info("Fetching user deck data...");
			UserDeckData userDeckData = this.client.getUserDeckData();
			MazeUtils.LOGGER.info("Switching to defense deck {} to complete maze.", userDeckData.getDefenceGroupid());
			this.client.setActiveDeckId(userDeckData.getDefenceGroupid().toString());
			List<MazeAward> awardList = new ArrayList<MazeAward>();
			if (playMode.equals(MazePlayMode.LEAVEONE)) {
				MazeUtils.LOGGER.info("Playing maze {} with {} only, starting with floor {}", mazeInfoData.getName(), playMode.name(), MazeUtils.DEFAULT_STARTING_FLOOR);
				awardList = this.playMazeFloorsLeaveOne(mapStageId, MazeUtils.DEFAULT_STARTING_FLOOR, MazePlayMode.LEAVEONE);
			}
			else if (playMode.equals(MazePlayMode.FINISH)) {
				MazeUtils.LOGGER.info("Playing maze {} trying for {}, starting with floor {}", mazeInfoData.getName(), playMode.name(), MazeUtils.DEFAULT_STARTING_FLOOR);
				awardList = this.playMazeFloorsLeaveOne(mapStageId, MazeUtils.DEFAULT_STARTING_FLOOR, MazePlayMode.FINISH);
			}
			else {
				MazeUtils.LOGGER.info("Playing maze {} with {} only, starting with floor {}", mazeInfoData.getName(), playMode.name(), MazeUtils.DEFAULT_STARTING_FLOOR);
				awardList = this.playMazeFloors(mapStageId, MazeUtils.DEFAULT_STARTING_FLOOR, playMode);
			}
			this.displayRewards(awardList);
			this.displayUserInfo();
		}
	}

	private void displayUserInfo() throws IOException {
		final UserInfo userInfo = this.client.getUserInfo();
		System.out.println(String.format("%d energy left", userInfo.getEnergy()));
	}

	private List<MazeAward> playMazeFloors(final Integer mapStageId, final Integer startingFloor, final MazePlayMode playMode) throws IOException, InterruptedException {
		final List<MazeAward> mazeAwardList = new ArrayList<MazeAward>();
		MazeFloorInfoData mazeFloorInfoData = this.client.showMazeFloorInfoData(mapStageId, startingFloor);
		final int totalFloors = mazeFloorInfoData.getTotalLayer();
		try {
			for (int floor = startingFloor; floor <= totalFloors; ++floor) {
				final List<Integer> floorItems = mazeFloorInfoData.getMap().getItems();
				final int floorBossIndex = floorItems.indexOf(MazeUtils.FLOOR_BOSS);
				final boolean floorFinished = mazeFloorInfoData.getMap().getIsFinish();
				final boolean clearChests = MazePlayMode.CHESTS.equals(playMode) && mazeFloorInfoData.getRemainBoxNum() > 0;
				final boolean clearMonsters = MazePlayMode.MONSTERS.equals(playMode) && mazeFloorInfoData.getRemainMonsterNum() > 0;
				final boolean lastFloor = floor == totalFloors;
				final boolean itemsOnFloor = mazeFloorInfoData.getRemainBoxNum() > 0 || mazeFloorInfoData.getRemainMonsterNum() > 0;
				MazeUtils.LOGGER.info("We have {} remaining chests and {} remaining monsters on floor {}", mazeFloorInfoData.getRemainBoxNum(), mazeFloorInfoData.getRemainMonsterNum(), floor);
				if (itemsOnFloor) {
					if (!lastFloor) {
						for (int index = 0; index < floorItems.size(); ++index) {
							if (MazeUtils.CHEST.equals(floorItems.get(index)) || MazeUtils.MONSTER.equals(floorItems.get(index))) {
								final String enemy = MazeUtils.CHEST.equals(floorItems.get(index)) ? "chest" : "monster";
								MazeUtils.LOGGER.info("Found a {} to kill at {}", enemy, index);
								final MazeAward award = this.client.doMazeBattle(mapStageId, floor, index);
								mazeAwardList.add(award);
							}
						}
						MazeUtils.LOGGER.info("Finished chests and monsters, searching for next floor boss");
					}
					else {
						Integer itemToKill = -1;
						if (clearChests) {
							itemToKill = MazeUtils.CHEST;
						}
						else if (clearMonsters) {
							itemToKill = MazeUtils.MONSTER;
						}
						if (itemToKill > 0) {
							final String enemy = MazeUtils.CHEST.equals(itemToKill) ? "chest" : "monster";
							for (int index2 = 0; index2 < floorItems.size(); ++index2) {
								if (itemToKill.equals(floorItems.get(index2))) {
									MazeUtils.LOGGER.info("Found a {} to kill at {}", enemy, index2);
									final MazeAward award2 = this.client.doMazeBattle(mapStageId, floor, index2);
									mazeAwardList.add(award2);
								}
							}
						}
					}
				}
				if (lastFloor) {
					MazeUtils.LOGGER.info("Meh, no level boss here, maze already finished");
					break;
				}
				if (floorFinished) {
					MazeUtils.LOGGER.info("Floor {} finished previously, climbing the stairs freely", floor);
				}
				else {
					MazeUtils.LOGGER.info("Trying to pass next floor, fighting with {} level boss", floor);
					final MazeAward award3 = this.client.doMazeBattle(mapStageId, floor, floorBossIndex);
					mazeAwardList.add(award3);
				}
				mazeFloorInfoData = this.client.showMazeFloorInfoData(mapStageId, floor + 1);
			}
		}
		catch (IllegalStateException ie) {
			MazeUtils.LOGGER.warn(ie.getMessage());
		}
		return mazeAwardList;
	}

	private List<MazeAward> playMazeFloorsLeaveOne(final Integer mapStageId, final Integer startingFloor, final MazePlayMode playMode) throws IOException, InterruptedException {
		final List<MazeAward> mazeAwardList = new ArrayList<MazeAward>();
		MazeFloorInfoData mazeFloorInfoData = this.client.showMazeFloorInfoData(mapStageId, startingFloor);
		final int totalFloors = mazeFloorInfoData.getTotalLayer();
		boolean leaveOneSucceeded = false;
		String leaveOneMessage = "";
		try {
			int floor = startingFloor;
			while (floor <= totalFloors) {
				final List<Integer> floorItems = mazeFloorInfoData.getMap().getItems();
				final int floorBossIndex = floorItems.indexOf(MazeUtils.FLOOR_BOSS);
				final boolean floorFinished = mazeFloorInfoData.getMap().getIsFinish();
				final boolean remainingChests = mazeFloorInfoData.getRemainBoxNum() > 0;
				final boolean remainingMonsters = mazeFloorInfoData.getRemainMonsterNum() > 0;
				final boolean remainingItems = remainingChests || remainingMonsters;
				final boolean lastFloor = floor == totalFloors;
				if (remainingItems) {
					MazeUtils.LOGGER.info("We have {} remaining chests and {} monsters on floor {}", mazeFloorInfoData.getRemainBoxNum(), mazeFloorInfoData.getRemainMonsterNum(), floor);
					final int totalItemsOnFloor = mazeFloorInfoData.getRemainBoxNum() + mazeFloorInfoData.getRemainMonsterNum();
					int clearedItems = 0;
					for (int index = 0; index < floorItems.size(); ++index) {
						if (playMode.equals(MazePlayMode.LEAVEONE) && lastFloor && !leaveOneSucceeded && clearedItems == totalItemsOnFloor - 1) {
							MazeUtils.LOGGER.info("Showing mercy for the last one");
							leaveOneSucceeded = true;
							leaveOneMessage = "Success. One item left on floor " + floor;
							break;
						}
						if (MazeUtils.CHEST.equals(floorItems.get(index)) || MazeUtils.MONSTER.equals(floorItems.get(index))) {
							MazeUtils.LOGGER.info("Found something to kill at {}", index);
							final MazeAward award = this.client.doMazeBattle(mapStageId, floor, index);
							mazeAwardList.add(award);
							++clearedItems;
						}
					}
				}
				if (lastFloor) {
					MazeUtils.LOGGER.info("Meh, no level boss here, maze already finished");
					if (leaveOneSucceeded) {
						MazeUtils.LOGGER.info(leaveOneMessage);
						break;
					}
					break;
				}
				else {
					if (floorFinished) {
						MazeUtils.LOGGER.info("Floor {} finished previously, climbing the stairs freely", floor);
					}
					else {
						MazeUtils.LOGGER.info("Trying to pass next floor, fighting with {} level boss", floor);
						final MazeAward award2 = this.client.doMazeBattle(mapStageId, floor, floorBossIndex);
						mazeAwardList.add(award2);
					}
					mazeFloorInfoData = this.client.showMazeFloorInfoData(mapStageId, floor + 1);
					++floor;
				}
			}
		}
		catch (IllegalStateException ie) {
			MazeUtils.LOGGER.warn(ie.getMessage());
		}
		return mazeAwardList;
	}

	private void displayRewards(final List<MazeAward> awardList) {
		if (awardList == null || awardList.isEmpty()) {
			return;
		}
		final List<String> cardIdList = new ArrayList<String>();
		final List<MazeAwardCardChip> cardFragmentList = new ArrayList<MazeAwardCardChip>();
		int totalExp = 0;
		int totalGold = 0;
		for (final MazeAward award : awardList) {
			final List<String> drops = award.getSecondDrop();
			if (drops != null && !drops.isEmpty()) {
				for (final String cardId : drops) {
					cardIdList.add(cardId);
				}
			}
			if (award.getCardChip() != null) {
				cardFragmentList.add(award.getCardChip());
			}
			if (award.getExp() != null)
				totalExp += award.getExp();
			if (award.getCoins() != null)
				totalGold += award.getCoins();
		}
		final String chestDrops = EkUtils.printCardDrops(cardIdList, cardFragmentList);
		System.out.println(chestDrops);
		System.out.println("Total Exp:  " + totalExp);
		System.out.println("Total Gold: " + totalGold);
	}

	public void printMazeStatus() throws IOException {
		final UserInfo userInfo = this.client.getUserInfo();
		final Integer buyEnergyCost = userInfo.getBuyEnergyCost();
		final Map<Integer, MazeStats> mazeInfoMap = this.initializeMazeInfos();
		int[] mapStages = new int[] { 2, 3, 4, 5, 6, 7, 8, 9, 10, 99 };
		for (int mapStageId : mapStages) {
			final MazeStats currentMaze = mazeInfoMap.get(mapStageId);
			final MazeInfoData mazeInfoData = this.getMazeInfo(mapStageId);
			if (mazeInfoData == null) {
				continue;
			}

			currentMaze.setMapStageId(mapStageId);
			currentMaze.setMazeName(mazeInfoData.getName());
			currentMaze.setCanReset(MazeUtils.CAN_RESET_MAZE.equals(mazeInfoData.getFreeReset()));
			currentMaze.setCanClear(MazeUtils.CAN_CLEAR_MAZE.equals(mazeInfoData.getClear()));
			currentMaze.setResetPrice(mazeInfoData.getResetCash());
			if (MazeUtils.MAZE_ALREADY_CLEARED.equals(mazeInfoData.getClear())) {
				currentMaze.setCurrentFloor(1);
				currentMaze.setRemainingChestsTillCurrentFloor(0);
				currentMaze.setRemainingMonstersTillCurrentFloor(0);
			}
			else {
				final int startingFloor = 1;
				MazeFloorInfoData mazeFloorInfoData = this.client.showMazeFloorInfoData(mapStageId, startingFloor);
				final int totalFloors = mazeFloorInfoData.getTotalLayer();
				int floor = startingFloor;
				while (floor <= totalFloors) {
					final boolean lastFloor = floor == totalFloors;
					currentMaze.setChestsInFloor(floor, mazeFloorInfoData.getRemainBoxNum());
					currentMaze.setMonstersInFloor(floor, mazeFloorInfoData.getRemainMonsterNum());
					if (mazeFloorInfoData.getMap().getIsFinish().equals(Boolean.FALSE)) {
						if (!lastFloor) {
							break;
						}
						break;
					}
					else {
						if (!lastFloor) {
							currentMaze.setRemainingStairs(currentMaze.getRemainingStairs() - 1);
							mazeFloorInfoData = this.client.showMazeFloorInfoData(mapStageId, floor + 1);
						}
						++floor;
					}
				}
			}
		}
		this.printMazeInfos(mazeInfoMap, buyEnergyCost);
	}

	private void printMazeInfos(final Map<Integer, MazeStats> mazeInfoMap, final Integer buyEnergyCost) {
		for (final Map.Entry<Integer, MazeStats> mazeStat : mazeInfoMap.entrySet()) {
			System.out.println("Key = " + mazeStat.getKey() + ", Value = " + mazeStat.getValue());
		}
		final Comparator<? super Map.Entry<Integer, MazeStats>> byEnergyPerChests = (e1, e2) -> e1.getValue().getEnergyPerChest().compareTo(e2.getValue().getEnergyPerChest());
		final DecimalFormat df = new DecimalFormat("#.00");
		System.out.println("No chests in these mazes");
		mazeInfoMap.entrySet().stream().filter(e -> e.getValue().getRemainingChestsInFloors(1) == 0).forEach(e -> System.out.println("Maze: " + e.getKey()));
		System.out.println("Your current energy buying cost: " + buyEnergyCost);
		System.out.println("Energy per gems: " + df.format(buyEnergyCost / MazeUtils.ENERGYPERBUY));
		System.out.println(UtilCons.NL + "Maze clear order");
		mazeInfoMap.entrySet().stream()
			.filter(e -> e.getValue().isCanClear())
			.filter(e -> e.getValue().getRemainingChestsInFloors(1) > 0)
			.sorted(byEnergyPerChests)
			.forEach(e -> System.out.println("Map: " + e.getKey() + " -> " + df.format(e.getValue().getEnergyPerChest()) + " energy per chest " + e.getValue()));
	}

	private Map<Integer, MazeStats> initializeMazeInfos() {
		final Map<Integer, MazeStats> mazeInfoMap = new HashMap<Integer, MazeStats>();
		mazeInfoMap.put(2, new MazeStats(3, new int[] { 2, 2, 2 }, new int[] { 1, 1, 2 }));
		mazeInfoMap.put(3, new MazeStats(3, new int[] { 2, 2, 3 }, new int[] { 1, 1, 2 }));
		mazeInfoMap.put(4, new MazeStats(4, new int[] { 2, 2, 2, 2 }, new int[] { 1, 1, 2, 2 }));
		mazeInfoMap.put(5, new MazeStats(4, new int[] { 2, 2, 2, 3 }, new int[] { 1, 1, 2, 2 }));
		mazeInfoMap.put(6, new MazeStats(5, new int[] { 2, 2, 2, 2, 2 }, new int[] { 1, 1, 2, 2, 3 }));
		mazeInfoMap.put(7, new MazeStats(5, new int[] { 2, 2, 2, 2, 2 }, new int[] { 1, 1, 2, 2, 3 }));
		mazeInfoMap.put(8, new MazeStats(5, new int[] { 2, 2, 2, 2, 2 }, new int[] { 1, 1, 2, 2, 3 }));
		mazeInfoMap.put(9, new MazeStats(5, new int[] { 2, 2, 2, 2, 3 }, new int[] { 1, 1, 2, 2, 2 }));
		mazeInfoMap.put(10, new MazeStats(5, new int[] { 2, 2, 2, 2, 4 }, new int[] { 1, 1, 2, 2, 1 }));
		mazeInfoMap.put(99, new MazeStats(5, new int[] { 5, 5, 5, 5, 5 }, new int[] { 0, 0, 0, 0, 0 }));
		return mazeInfoMap;
	}

	static {
		LOGGER = LogManager.getLogger();
		CAN_CLEAR_MAZE = 0;
		MAZE_ALREADY_CLEARED = 1;
		CAN_RESET_MAZE = 1;
		MAZE_ALREADY_RESET = 0;
		CHEST = 2;
		MONSTER = 3;
		FLOOR_BOSS = 5;
		DEFAULT_STARTING_FLOOR = 1;
		MazeUtils.ENERGYPERBUY = 20.0;
	}
}
