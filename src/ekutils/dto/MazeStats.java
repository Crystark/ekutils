// 
// EkUtils 
// 

package ekutils.dto;

import java.util.Arrays;

public class MazeStats
{
    private int mapStageId;
    private String mazeName;
    private boolean canReset;
    private boolean canClear;
    private int resetPrice;
    private int remainingChestsTillCurrentFloor;
    private int remainingMonstersTillCurrentFloor;
    private int totalFloors;
    private int remainingStairs;
    private int[] chestsByFloor;
    private int[] monstersByFloor;
    private int currentFloor;
    private static int ENERGYPERITEM;
    
    public MazeStats(final int totalFloors, final int[] chestsByFloor, final int[] monstersByFloor) {
        this.totalFloors = totalFloors;
        this.remainingStairs = totalFloors - 1;
        this.chestsByFloor = chestsByFloor;
        this.monstersByFloor = monstersByFloor;
    }
    
    public int getRemainingChestsInFloors(final int startingFloor) {
        int total = 0;
        for (int i = startingFloor; i <= this.totalFloors; ++i) {
            total += this.chestsByFloor[i - 1];
        }
        return total;
    }
    
    public int getRemainingMonstersInFloors(final int startingFloor) {
        int total = 0;
        for (int i = startingFloor; i <= this.totalFloors; ++i) {
            total += this.monstersByFloor[i - 1];
        }
        return total;
    }
    
    public String getMazeSummary() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Floors: ").append(this.totalFloors).append(",");
        sb.append("Chests: ").append(this.getRemainingChestsInFloors(1)).append(",");
        sb.append("Monsters: ").append(this.getRemainingMonstersInFloors(1));
        return sb.toString();
    }
    
    public Double getEnergyPerChest() {
        final int remainingChests = this.getRemainingChestsInFloors(1);
        if (remainingChests == 0) {
            return 0.0;
        }
        final Double energyPerChest = (Double)((remainingChests + this.remainingStairs) * MazeStats.ENERGYPERITEM / remainingChests * 1.0);
        return energyPerChest;
    }
    
    public Double getGemsPerChest() {
        final int remainingChests = this.getRemainingChestsInFloors(1);
        if (remainingChests == 0) {
            return 0.0;
        }
        //final int actualResetPrice = this.canClear ? 0 : this.resetPrice;
        //final int energyNeededWithoutMonsters = (remainingChests + this.remainingStairs) * MazeStats.ENERGYPERITEM;
        final double energyPrice = 2.5;
        return energyPrice;
    }
    
    public void setChestsInFloor(final int floor, final int value) {
        this.chestsByFloor[floor - 1] = value;
    }
    
    public void setMonstersInFloor(final int floor, final int value) {
        this.monstersByFloor[floor - 1] = value;
    }
    
    public int getChestInFloor(final int floor) {
        return this.chestsByFloor[floor - 1];
    }
    
    public int getMonstersInFloor(final int floor) {
        return this.monstersByFloor[floor - 1];
    }
    
    public boolean isCanReset() {
        return this.canReset;
    }
    
    public void setCanReset(final boolean canReset) {
        this.canReset = canReset;
    }
    
    public boolean isCanClear() {
        return this.canClear;
    }
    
    public void setCanClear(final boolean canClear) {
        this.canClear = canClear;
    }
    
    public int getMapStageId() {
        return this.mapStageId;
    }
    
    public void setMapStageId(final int mapStageId) {
        this.mapStageId = mapStageId;
    }
    
    public String getMazeName() {
        return this.mazeName;
    }
    
    public void setMazeName(final String mazeName) {
        this.mazeName = mazeName;
    }
    
    public int getResetPrice() {
        return this.resetPrice;
    }
    
    public void setResetPrice(final int resetPrice) {
        this.resetPrice = resetPrice;
    }
    
    public int getCurrentFloor() {
        return this.currentFloor;
    }
    
    public void setCurrentFloor(final int currentFloor) {
        this.currentFloor = currentFloor;
    }
    
    public int getRemainingChestsTillCurrentFloor() {
        return this.remainingChestsTillCurrentFloor;
    }
    
    public void setRemainingChestsTillCurrentFloor(final int remainingChestsTillCurrentFloor) {
        this.remainingChestsTillCurrentFloor = remainingChestsTillCurrentFloor;
    }
    
    public int getRemainingMonstersTillCurrentFloor() {
        return this.remainingMonstersTillCurrentFloor;
    }
    
    public void setRemainingMonstersTillCurrentFloor(final int remainingMonstersTillCurrentFloor) {
        this.remainingMonstersTillCurrentFloor = remainingMonstersTillCurrentFloor;
    }
    
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("MazeStats [mapStageId=");
        builder.append(this.mapStageId);
        builder.append(", remainingStairs=");
        builder.append(this.getRemainingStairs());
        builder.append(", Summary=");
        builder.append(this.getMazeSummary());
        builder.append(", mazeName=");
        builder.append(this.mazeName);
        builder.append(", canReset=");
        builder.append(this.canReset);
        builder.append(", canClear=");
        builder.append(this.canClear);
        builder.append(", resetPrice=");
        builder.append(this.resetPrice);
        builder.append(", chestsByFloor=");
        builder.append(Arrays.toString(this.chestsByFloor));
        builder.append(", monstersByFloor=");
        builder.append(Arrays.toString(this.monstersByFloor));
        builder.append(", currentFloor=");
        builder.append(this.currentFloor);
        builder.append("]");
        return builder.toString();
    }
    
    public int getRemainingStairs() {
        return this.remainingStairs;
    }
    
    public void setRemainingStairs(final int remainingStairs) {
        this.remainingStairs = remainingStairs;
    }
    
    static {
        MazeStats.ENERGYPERITEM = 2;
    }
}
