// 
// EkUtils 
// 

package ekutils.dto;

public enum MazePlayMode
{
    CHESTS, 
    STAIRS, 
    LEAVEONE, 
    MONSTERS, 
    FINISH;
}
