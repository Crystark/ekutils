//
// EkUtils
//

package ekutils;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import servconn.client.ClientFactory;
import servconn.client.EkClient;
import servconn.client.UserClient;
import servconn.dto.arena.Competitor;
import servconn.dto.battle.BattleData;
import servconn.dto.battle.Player;
import servconn.dto.card.Card;
import servconn.dto.rune.Rune;
import servconn.dto.skill.Skill;
import servconn.sim.FoHPrinter;
import servconn.sim.FoHPrinterFactory;
import servconn.util.Constants;

public class ArenaDeckUtils {
	private String					email;
	private String					password;
	private String					playerServer;
	private int						start;
	private int						amount;
	private TreeMap<String, String>	evoNames;

	public ArenaDeckUtils() {
		this.start = 1;
		this.amount = 100;
	}

	public ArenaDeckUtils(final String email, final String password, final String playerServer) {
		this();
		this.email = email;
		this.password = password;
		this.playerServer = playerServer;
	}

	public String printArenaDecks() throws IOException, InterruptedException {
		final EkClient guestclient = ClientFactory.get();
		final Map<String, Card> cardMap = guestclient.getServerCards(this.playerServer);
		final Map<String, Skill> skillMap = guestclient.getServerSkills(this.playerServer);
		final Map<String, Rune> runeMap = guestclient.getServerRunes(this.playerServer);
		final FoHPrinter fohPrinter = FoHPrinterFactory.getFoHPrinter("MITZI");
		fohPrinter.setGameInfo(cardMap, skillMap, runeMap);
		fohPrinter.setEvoNames(this.evoNames);
		final UserClient client = ClientFactory.get(this.playerServer, this.email, this.password);
		if (this.amount > 100) {
			this.amount = 100;
		}
		final List<Competitor> users = client.getRankUsers(this.start, this.amount);
		final StringBuilder deckBuilder = new StringBuilder();
		for (int i = 0; i < users.size(); ++i) {
			final Competitor user = users.get(i);
			final BattleData battleData = client.getFriendlyBattleData(user.getUid());
			final Player player = battleData.getData().getDefendPlayer();
			final String playerDeck = fohPrinter.printDeck(player);
			deckBuilder.append(playerDeck);
			if (i != users.size() - 1) {
				deckBuilder.append(Constants.NL);
			}
			Thread.sleep(5000L);
		}
		System.out.println(deckBuilder.toString());
		return deckBuilder.toString();
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(final String password) {
		this.password = password;
	}

	public String getPlayerServer() {
		return this.playerServer;
	}

	public void setPlayerServer(final String playerServer) {
		this.playerServer = playerServer;
	}

	public int getStart() {
		return this.start;
	}

	public void setStart(final int start) {
		this.start = start;
	}

	public int getAmount() {
		return this.amount;
	}

	public void setAmount(final int amount) {
		this.amount = amount;
	}

	public TreeMap<String, String> getEvoNames() {
		return this.evoNames;
	}

	public void setEvoNames(final TreeMap<String, String> evoNames) {
		this.evoNames = evoNames;
	}
}
