package ekutils;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.base.Charsets;
import com.google.common.io.Files;

import servconn.client.UserClient;
import servconn.client.UserClient;

public class ClaimUtils
{
    static final Logger LOGGER;
    private String argument;
    private UserClient client;
    
    public ClaimUtils(UserClient client) {
        this.client = client;
    }
    
    public void setArgument(final String claimGiftCodeArgument) {
        this.argument = claimGiftCodeArgument;
    }
    
    public void claimGiftCode() throws IOException, InterruptedException {
        final File file = new File(this.argument);
        if (file.exists() && !file.isDirectory()) {
            ClaimUtils.LOGGER.info("Reading gift codes from file {}", file.getAbsolutePath());
            final List<String> lines = Files.readLines(file, Charsets.UTF_8);
            for (final String line : lines) {
                this.claimGiftCode(line);
                Thread.sleep(200L);
            }
        }
        else {
            this.claimGiftCode(this.argument);
        }
    }
    
    private void claimGiftCode(final String giftCode) throws IOException {
        try {
            this.client.claimGiftCode(giftCode);
            ClaimUtils.LOGGER.info("Claimed gift code {} with success, check your inbox", giftCode);
        }
        catch (IllegalStateException ise) {
            ClaimUtils.LOGGER.warn("{} failed: {}", giftCode, ise.getMessage());
        }
    }
    
    static {
        LOGGER = LogManager.getLogger();
    }
}
